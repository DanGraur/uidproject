/**
 * Created by noi on 12/26/2016.
 */

$.getScript("/resources/js/progress/minified/progress.min.js");

function searchBarFunction() {
    document.getElementById('search-results').innerHTML = "";

    if ($(this).val() != "") {
        $.get("/general/search-bar?query=" + $(this).val(), function(responseJson) {
            $.each(responseJson, function(index, result) {

                var $aImage = $("<a>", {
                    href: result.hrefUrl
                });

                var $img = $("<img>", {
                    src: result.imageUrl,
                    class: "adjusted-image"
                });

                $img.appendTo($aImage);

                var $aName = $("<a>", {
                    href: result.hrefUrl
                });

                $aName.append(result.text);

                var $li = $("<li>");

                $li.append($aImage);
                $li.append($aName);

                $li.appendTo(document.getElementById('search-results'));

            });

            document.getElementById('search-results').style.display = 'block';
        });
    } else
        document.getElementById('search-results').style.display = 'none';
}

$(function() {

    progressJs().start().autoIncrease(10, 50);

    setTimeout(function() {
        progressJs().end();
    }, 500);


    $('#search-input').keyup(searchBarFunction);

});

function recommendFilmPopUp() {
    $.get({
        url: "/general/username",
        success: function (username) {
            if (username == "")
                $( "#login-dialog" ).dialog({
                    position: {
                        my: "top",
                        at: "top",
                        of: window
                    },
                    width: 315
                });
            else {
                $( "#recommend-to-friend-dialog" ).dialog({
                    position: {
                        my: "top",
                        at: "top",
                        of: window
                    },
                    width: 600
                });
            }
        },
        error: function () {
            $("#error-dialog").dialog({
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },
                width: 315
            });
        }
    });

}

function recommendFilm(filmName) {
    $.get("/film/recommend?name=" + $( '#friend-name-select' ).val() + '&filmName=' + filmName, function () {
        $( "#recommend-to-friend-dialog" ).dialog('close');

        $( "#success-dialog" ).dialog({
            position: {
                my: "top",
                at: "top",
                of: window
            },
            buttons: {
                Ok: function() {
                    $( this ).dialog( "close" );
                }
            },
            width: 330
        });

    });
}

function filmFavorite(element, filmName) {

    $.ajax({
        url: "/general/username",
        success: function (username) {
            if (username == "")
                $( "#login-dialog" ).dialog({
                    position: {
                        my: "top",
                        at: "top",
                        of: window
                    },
                    width: 315
                });
            else {
                $.get("/general/favorite-film?name=" + filmName);

                var src = $(element).attr('src').split("/");

                if (src[src.length - 1] == 'wishlist.png')
                    $(element).attr('src', '/resources/images/icons/correct.png');
                else
                    $(element).attr('src', '/resources/images/icons/wishlist.png');
            }
        },
        error: function () {
            $("#error-dialog").dialog({
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },
                width: 315
            });
        }
    });

}

function actorFavorite(element, actorName) {

    $.ajax({
        url:"/general/username",
        success: function (username) {
            if (username == "")
                $( "#login-dialog" ).dialog({
                    position: {
                        my: "top",
                        at: "top",
                        of: window
                    },
                    width: 315
                });
            else {
                $.get("/general/favorite-actor?name=" + actorName);

                var src = $(element).attr('src').split("/");

                if (src[src.length - 1] == 'wishlist.png')
                    $(element).attr('src', '/resources/images/icons/correct.png');
                else
                    $(element).attr('src', '/resources/images/icons/wishlist.png');

                $( "#success-dialog" ).dialog({
                    position: {
                        my: "top",
                        at: "top",
                        of: window
                    },
                    buttons: {
                        Ok: function() {
                            $( this ).dialog( "close" );
                        }
                    },
                    width: 330
                });
            }
        },
        error: function () {
            $("#error-dialog").dialog({
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },
                width: 315
            });
        }
    });

}

function checkNotification() {
    $.ajax({
        url: '/general/notification',
        success: function (data) {
            $('#notification-nav').empty()

            if (data != "") {
                var $aImage = $("<a>", {
                    href: "/account/notifications",
                    title: 'You have notification that need attention'
                });

                var $img = $("<img>", {
                    src: "/resources/images/icons/notification.png",
                    class: "adjusted-image"
                });

                $img.appendTo($aImage);

                $('#notification-nav').append($aImage);
            }
        },
        complete: function () {
            // Schedule the next request when the current one's complete
            setTimeout(checkNotification, 10000);
        },
        error: function() {

        }
    });
}

$(function () {
    checkNotification();
});

function ticketOptionSelector() {
    $.ajax({
        url: "/general/username",
        success: function (username) {
            if (username == "")
                $( "#login-dialog" ).dialog({
                    position: {
                        my: "top",
                        at: "top",
                        of: window
                    },
                    width: 315
                });
            else {
                $.get("/general/tickets/cinemas", function(options) {
                    var $cinemas = $('#ticket-cinema-id');
                    $cinemas.empty();

                    $('#ticket-date-id').empty();
                    $('#ticket-hour-id').empty();
                    $('#ticket-button').hide();

                    $.each(options, function(index, result) {
                        var $aOption = $("<option>", {
                            value: result
                        });

                        $aOption.append(result);

                        $cinemas.append($aOption);
                    });

                    $( "#buy-tickets-dialog" ).dialog({
                        position: {
                            my: "top",
                            at: "top",
                            of: window
                        },
                        width: 840
                    });
                });
            }
        },
        error: function () {
            $("#error-dialog").dialog({
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },
                width: 315
            });
        }
    });
}

function changeDates() {
    $.ajax({
        url: "/general/tickets/date",
        data: { name: $('#ticket-cinema-id').val() },
        success: function(options) {
            var $dates = $('#ticket-date-id');
            $dates.empty();

            $('#ticket-hour-id').empty();
            $('#ticket-button').hide();

            $.each(options, function(index, result) {
                var $aOption = $("<option>", {
                    value: result
                });

                $aOption.append(result);

                $dates.append($aOption);
            });
        },
        error: function () {
            $("#error-dialog").dialog({
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },
                width: 315
            });
        }
    });
}

function changeHours() {
    $.ajax({
        url: "/general/tickets/hour",
        data: { name: $('#ticket-date-id').val()},
        success: function(options) {
            var $hours = $('#ticket-hour-id');
            $hours.empty();
            $('#ticket-button').hide();

            $.each(options, function(index, result) {
                var $aOption = $("<option>", {
                    value: result
                });

                $aOption.append(result);

                $hours.append($aOption);
            });
        },
        error: function () {
            $("#error-dialog").dialog({
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },
                width: 315
            });
        }
    });
}

function enableTicketButton() {
    $('#ticket-button').show();
}

function buyTickets() {
    $( "#buy-tickets-dialog" ).dialog('close');

    $.ajax({
        url: "/general/username",
            success: function (username) {
            if (username == "")
                $( "#login-dialog" ).dialog({
                    position: {
                        my: "top",
                        at: "top",
                        of: window
                    },
                    width: 315
                });
            else {
                $.ajax({
                    url: '/general/tickets/send-data',
                    data: {
                        cinema: $('#ticket-cinema-id').val(),
                        date: $('#ticket-date-id').val(),
                        hour: $('#ticket-hour-id').val(),
                        count: $('#ticket-count-id').val()
                    },
                    success: function(info) {
                        $( "#user-name" ).val(info.name)
                        $( "#card-number" ).val(info.cardNumber);

                        $( "#card-information-dialog" ).dialog({
                            position: {
                                my: "top",
                                at: "top",
                                of: window
                            },
                            width: 375
                        });
                    }
                })
            }
        },
        error: function () {
            $("#error-dialog").dialog({
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },
                width: 315
            });
        }
    });
}

function submitCardInfo() {
    if ($('#user-name').val() == "") {
        alert('Username cannot be empty!');

        return;
    }

    if ($('#card-number').val() == "") {
        alert('Card Number cannot be empty!');

        return;
    }

    $.ajax({
        url: "/general/username",
        success: function (username) {
            if (username == "")
                $("#login-dialog").dialog({
                    position: {
                        my: "top",
                        at: "top",
                        of: window
                    },
                    width: 315
                });
            else {
                $.ajax({
                    url: '/general/tickets/send-card-info',
                    data: {
                        username: $('#user-name').val(),
                        cardNumber: $('#card-number').val()
                    },
                    success: function () {
                        $("#card-information-dialog").dialog('close');

                        $("#success-dialog").dialog({
                            position: {
                                my: "top",
                                at: "top",
                                of: window
                            },
                            buttons: {
                                Ok: function () {
                                    $(this).dialog("close");
                                }
                            },
                            width: 330
                        });
                    }
                });
            }
        },
        error: function () {
            $("#error-dialog").dialog({
                position: {
                    my: "top",
                    at: "top",
                    of: window
                },
                width: 315
            });
        }
    });
}