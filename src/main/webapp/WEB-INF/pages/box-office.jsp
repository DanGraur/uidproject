<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<%--
  Created by IntelliJ IDEA.
  User: noi
  Date: 12/24/2016
  Time: 12:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<form name = "logoutForm" id = "logoutForm" action="<c:url value="/general/logout" />"></form>

<script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery-ui-1.12.1.custom/jquery-ui.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery.qtip.min.js" />"></script>
<script src="<c:url value="/resources/js/common.js" />?v=1.2"></script>

<div id="login-dialog" class = "pop-up-dialog" title="Please Log In First">
    <p>
        You must first <a href = "<c:url value="/login" />">Log In</a> to use this feature
    </p>
</div>

<div id="error-dialog" class = "pop-up-dialog" title="Oops!">
    <p>
        <b>Oops! Looks like we've got some technical issues! Try again later...</b>
    </p>
</div>

<script>

    <!-- Attach Tooltips -->
    $(document).ready(function()
    {
        $('*[title]').each(function() {
            $(this).qtip({
                style: {
                    classes: 'qtip-tipsy'
                }
            });
        });
    });

</script>

<head>
    <title>Box Office</title>

    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/progress/minified/progressjs.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery.qtip.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery-ui-1.12.1.custom/jquery-ui.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/common.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/box-office.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/icon-effect.css" />" />
</head>
<body>
<header>
    <div>
        <a href="<c:url value="/home" />"><img src="<c:url value="/resources/images/common/logo.png" />"></a>
    </div><!--
            --><div>
    <nav>
        <ul>
            <li id = "notification-nav">
            </li>
            <li>
                <a title = "Go to Homepage" href="<c:url value="/home" />">Home</a>
            </li>
            <c:choose>
                <c:when test = "${empty sessionScope.username}">
                    <li>
                        <a title = "Go to Login" href="<c:url value="/login" />">Login</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a title = "Go to My Account" href="<c:url value="/account" />">My Account</a>
                    </li>
                    <li>
                        <a title = "Logout" href="#" onclick="document.logoutForm.submit()" >Logout</a>
                    </li>
                </c:otherwise>
            </c:choose>
            <li>
                <a title = "Go to Help Page" href="<c:url value="/help/general" />">Help</a>
            </li>
        </ul>
    </nav>
</div>
</header>
<form method = "get">
    <table>
        <tr>
            <td id = "icon-table-cell">
                <img id = "magnifier-image" src="<c:url value="/resources/images/common/magnifier.png" />">
            </td>
            <td id = "search-table-cell">
                <input type="text" id = "search-input" autocomplete="off" placeholder="Search for a movie, actor, director..."/>
            </td>
        </tr>
    </table>
    <div id = "absolute-div">
        <ul id = "search-results">
        </ul>
    </div>
</form>
<section class = "body-content">

    <!-- Content Start -->
    <div>
        <h2>
            Box Office:
        </h2>

        <table class = "box-office-table">
            <thead>
                <tr>
                    <th></th>
                    <th>Title</th>
                    <th>Weekend</th>
                    <th>Gross</th>
                    <th>Weeks</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/home/BoxOffice/inception.jpg" />"></a></td>
                    <td><a href="<c:url value="/film" />">Inception</a></td>
                    <td>$155.1M</td>
                    <td>$155.1M</td>
                    <td>1</td>
                    <td>
                        <c:set var="contains" value="false" />

                        <c:forEach var="item" items="${sessionScope.films}">
                            <c:if test="${item == 'inception'}">
                                <c:set var="contains" value="true" />
                            </c:if>
                        </c:forEach>

                        <c:choose>
                            <c:when test="${contains == true}">
                                <img title = "Add/Remove Film To Watchlist" class = "focus-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="filmFavorite(this, 'inception')">
                            </c:when>
                            <c:otherwise>
                                <img title = "Add/Remove Film To Watchlist" class = "focus-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="filmFavorite(this, 'inception')">
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td><a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/home/BoxOffice/blackswan.jpg" />"></a></td>
                    <td><a href="<c:url value="/film" />">Black Swan</a></td>
                    <td>$12.7M</td>
                    <td>$162.9M</td>
                    <td>4</td>
                    <td>
                        <c:set var="contains" value="false" />

                        <c:forEach var="item" items="${sessionScope.films}">
                            <c:if test="${item == 'black-swan'}">
                                <c:set var="contains" value="true" />
                            </c:if>
                        </c:forEach>

                        <c:choose>
                            <c:when test="${contains == true}">
                                <img title = "Add/Remove Film To Watchlist" class = "focus-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="filmFavorite(this, 'black-swan')">
                            </c:when>
                            <c:otherwise>
                                <img title = "Add/Remove Film To Watchlist" class = "focus-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="filmFavorite(this, 'black-swan')">
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td><a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/home/BoxOffice/sw.jpg" />"></a></td>
                    <td><a href="<c:url value="/film" />">Star Wars</a></td>
                    <td>$8.6M</td>
                    <td>$31.7M</td>
                    <td>2</td>
                    <td>
                        <c:set var="contains" value="false" />

                        <c:forEach var="item" items="${sessionScope.films}">
                            <c:if test="${item == 'sw'}">
                                <c:set var="contains" value="true" />
                            </c:if>
                        </c:forEach>

                        <c:choose>
                            <c:when test="${contains == true}">
                                <img title = "Add/Remove Film To Watchlist" class = "focus-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="filmFavorite(this, 'sw')">
                            </c:when>
                            <c:otherwise>
                                <img title = "Add/Remove Film To Watchlist" class = "focus-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="filmFavorite(this, 'sw')">
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td><a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/home/BoxOffice/fantastic-beasts.jpg" />"></a></td>
                    <td><a href="<c:url value="/film" />">Fantastic Beasts and Where to Find Them</a></td>
                    <td>$7.1M</td>
                    <td>$207.7M</td>
                    <td>5</td>
                    <td>
                        <c:set var="contains" value="false" />

                        <c:forEach var="item" items="${sessionScope.films}">
                            <c:if test="${item == 'fantastic-beasts'}">
                                <c:set var="contains" value="true" />
                            </c:if>
                        </c:forEach>

                        <c:choose>
                            <c:when test="${contains == true}">
                                <img title = "Add/Remove Film To Watchlist" class = "focus-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="filmFavorite(this, 'fantastic-beasts')">
                            </c:when>
                            <c:otherwise>
                                <img title = "Add/Remove Film To Watchlist" class = "focus-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="filmFavorite(this, 'fantastic-beasts')">
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td><a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/home/BoxOffice/rogue-one.jpg" />"></a></td>
                    <td><a href="<c:url value="/film" />">Rogue One: A Star Wars Story</a></td>
                    <td>$4.2M</td>
                    <td>$14.1M</td>
                    <td>5</td>
                    <td>
                        <c:set var="contains" value="false" />

                        <c:forEach var="item" items="${sessionScope.films}">
                            <c:if test="${item == 'rogue-one'}">
                                <c:set var="contains" value="true" />
                            </c:if>
                        </c:forEach>

                        <c:choose>
                            <c:when test="${contains == true}">
                                <img title = "Add/Remove Film To Watchlist" class = "focus-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="filmFavorite(this, 'rogue-one')">
                            </c:when>
                            <c:otherwise>
                                <img title = "Add/Remove Film To Watchlist" class = "focus-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="filmFavorite(this, 'rogue-one')">
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- Content End -->

</section>

<footer>
    <span>Copyright &copy; 2016 - 2017 Dan-Ovidiu Graur</span>
</footer>

</body>
</html>
