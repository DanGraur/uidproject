<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<%--
  Created by IntelliJ IDEA.
  User: noi
  Date: 12/24/2016
  Time: 12:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<form name = "logoutForm" id = "logoutForm" action="<c:url value="/general/logout" />"></form>

<script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery-ui-1.12.1.custom/jquery-ui.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery.qtip.min.js" />"></script>
<script src="<c:url value="/resources/js/progressbar.min.js" />"></script>
<script src="<c:url value="/resources/js/common.js" />?v=1.2"></script>
<script src="<c:url value="/resources/js/home.js" />"></script>

<div id="login-dialog" class = "pop-up-dialog" title="Please Log In First">
    <p>
        You must first <a href = "<c:url value="/login" />">Log In</a> to use this feature
    </p>
</div>

<div id="error-dialog" class = "pop-up-dialog" title="Oops!">
    <p>
        <b>Oops! Looks like we've got some technical issues! Try again later...</b>
    </p>
</div>

<script>

    <!-- Attach Tooltips -->
    $(document).ready(function()
    {
        /*
        var bar = new ProgressBar.Line('#container', {
            easing: 'easeInOut',
            color: '#FCB03C',
            finish: true
        });
        bar.animate(1);

        */


        $('*[title]').each(function() {
            $(this).qtip({
                style: {
                    classes: 'qtip-tipsy'
                }
            });
        });
    });


</script>


<div id="container"></div>

    <head>
        <title>Homepage</title>

        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery.qtip.min.css" />" />
        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery-ui-1.12.1.custom/jquery-ui.min.css" />" />
        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/progress/minified/progressjs.min.css" />" />
        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/icon-effect.css" />" />
        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/common.css" />?v=1.1" />
        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/home.css" />?v=1.1" />
    </head>
    <body>
        <header>
            <div>
                <a href="<c:url value="/home" />"><img src="<c:url value="/resources/images/common/logo.png" />"></a>
            </div><!--
            --><div>
                <nav>
                    <ul>
                        <li id = "notification-nav">
                        </li>
                        <li>
                            <a title = "Go to Homepage" href="<c:url value="/home" />">Home</a>
                        </li>
                        <c:choose>
                            <c:when test = "${empty sessionScope.username}">
                                <li>
                                    <a title = "Go to Login" href="<c:url value="/login" />">Login</a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li>
                                    <a title = "Go to My Account" href="<c:url value="/account" />">My Account</a>
                                </li>
                                <li>
                                    <a title = "Logout" href="#" onclick="document.logoutForm.submit()" >Logout</a>
                                </li>
                            </c:otherwise>
                        </c:choose>
                        <li>
                            <a title = "Go to Help Page" href="<c:url value="/help/general" />">Help</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
        <form method = "get">
            <table>
                <tr>
                    <td id = "icon-table-cell">
                        <img id = "magnifier-image" src="<c:url value="/resources/images/common/magnifier.png" />">
                    </td>
                    <td id = "search-table-cell">
                        <input type="text" id = "search-input" autocomplete="off" placeholder="Search for a movie, actor, director..."/>
                    </td>
                </tr>
            </table>
            <div id = "absolute-div">
                <ul id = "search-results">
                </ul>
            </div>
        </form>
        <section class = "body-content">

            <!-- Content Start -->
            <div class = "home-films-div">
                <h2>
                    Recent Movies:
                </h2>

                <div id = "absolute-div-home-pics">
                    <!-- Guardians Icon -->
                    <c:set var="contains" value="false" />

                    <c:forEach var="item" items="${sessionScope.films}">
                        <c:if test="${item == 'guardians'}">
                            <c:set var="contains" value="true" />
                        </c:if>
                    </c:forEach>

                    <c:choose>
                        <c:when test="${contains == true}">
                            <img title = "Add/Remove Film To Watchlist" id = "guardians-fav-icon" class = "focus-icon invisible-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="filmFavorite(this, 'guardians')">
                        </c:when>
                        <c:otherwise>
                            <img title = "Add/Remove Film To Watchlist" id = "guardians-fav-icon" class = "focus-icon invisible-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="filmFavorite(this, 'guardians')">
                        </c:otherwise>
                    </c:choose>

                    <!-- Prometheus Icon -->
                    <c:set var="contains" value="false" />

                    <c:forEach var="item" items="${sessionScope.films}">
                        <c:if test="${item == 'prometheus'}">
                            <c:set var="contains" value="true" />
                        </c:if>
                    </c:forEach>

                    <c:choose>
                        <c:when test="${contains == true}">
                            <img title = "Add/Remove Film To Watchlist" id = "prometheus-fav-icon" class = "focus-icon invisible-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="filmFavorite(this, 'prometheus')">
                        </c:when>
                        <c:otherwise>
                            <img title = "Add/Remove Film To Watchlist" id = "prometheus-fav-icon" class = "focus-icon invisible-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="filmFavorite(this, 'prometheus')">
                        </c:otherwise>
                    </c:choose>

                    <!-- Dr. Strange Icon -->
                    <c:set var="contains" value="false" />

                    <c:forEach var="item" items="${sessionScope.films}">
                        <c:if test="${item == 'strange'}">
                            <c:set var="contains" value="true" />
                        </c:if>
                    </c:forEach>

                    <c:choose>
                        <c:when test="${contains == true}">
                            <img title = "Add/Remove Film To Watchlist" id = "strange-fav-icon" class = "focus-icon invisible-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="filmFavorite(this, 'strange')">
                        </c:when>
                        <c:otherwise>
                            <img title = "Add/Remove Film To Watchlist" id = "strange-fav-icon" class = "focus-icon invisible-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="filmFavorite(this, 'strange')">
                        </c:otherwise>
                    </c:choose>


                    <!-- Interstellar Icon -->
                    <c:set var="contains" value="false" />

                    <c:forEach var="item" items="${sessionScope.films}">
                        <c:if test="${item == 'interstellar'}">
                            <c:set var="contains" value="true" />
                        </c:if>
                    </c:forEach>

                    <c:choose>
                        <c:when test="${contains == true}">
                            <img title = "Add/Remove Film To Watchlist" id = "interstellar-fav-icon" class = "focus-icon invisible-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="filmFavorite(this, 'interstellar')">
                        </c:when>
                        <c:otherwise>
                            <img title = "Add/Remove Film To Watchlist" id = "interstellar-fav-icon" class = "focus-icon invisible-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="filmFavorite(this, 'interstellar')">
                        </c:otherwise>
                    </c:choose>

                    <!-- Skyfall Icon -->
                    <c:set var="contains" value="false" />

                    <c:forEach var="item" items="${sessionScope.films}">
                        <c:if test="${item == 'skyfall'}">
                            <c:set var="contains" value="true" />
                        </c:if>
                    </c:forEach>

                    <c:choose>
                        <c:when test="${contains == true}">
                            <img title = "Add/Remove Film To Watchlist" id = "skyfall-fav-icon" class = "focus-icon invisible-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="filmFavorite(this, 'skyfall')">
                        </c:when>
                        <c:otherwise>
                            <img title = "Add/Remove Film To Watchlist" id = "skyfall-fav-icon" class = "focus-icon invisible-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="filmFavorite(this, 'skyfall')">
                        </c:otherwise>
                    </c:choose>
                </div>
                <div id = "first-film-div">
                    <a href="<c:url value="/film" />"><img class = "adjusted-image" src="<c:url value="/resources/images/home/guardians.jpg" />"></a>
                    <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/home/prometheus.jpg" />"></a>
                    <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/home/drStrange.jpeg" />"></a>
                </div><!--

                --><div id = "second-film-div">
                    <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/home/bond.jpg" />"></a>
                </div><!--

                --><div>
                    <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/home/interstellar.jpeg" />"></a>
                </div>
            </div>

            <div class = "news-section-div">
                <h2>
                    Recent News:
                </h2>

                <div class = "first-news-article">
                    <div>
                        <img src="<c:url value="/resources/images/home/actor.jpg" />">
                    </div><!--
                    --><div>
                        <h3>
                            <a href="<c:url value="/article" />">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a>
                        </h3>
                        <p>
                            21 minutes ago <a href="http://variety.com/">Variety</a>
                        </p>
                        <p>
                            Suspendisse ut semper neque. Quisque ac est mi. Nam cursus molestie ligula, ut consectetur dolor ultricies nec. Vivamus non sapien auctor, lobortis lorem vitae, feugiat sem. Curabitur scelerisque, massa eu rutrum hendrerit, eros nunc rutrum massa, id hendrerit nulla libero non orci. Nulla interdum auctor posuere. Mauris sapien justo, ornare id pellentesque eget, lacinia scelerisque orci. Nullam in volutpat nibh. Aliquam hendrerit tristique tellus nec vulputate <a title = "Read Full Article" href="<c:url value="/home" />">... read more</a>
                        </p>
                    </div>
                </div>

                <div>

                    <div class = "other-news-article-div">
                        <div class = "regular-news-article-div">
                            <h3>
                                <a href="<c:url value="/article" />">Praesent accumsan libero tincidunt dolor pulvinar porttitor. Morbi quis facilisis</a>
                            </h3>
                            <p>
                                21 minutes ago <a href="http://variety.com/">Variety</a>
                            </p>
                        </div><!--

                        --><div class = "regular-news-article-div">
                            <h3>
                                <a href="<c:url value="/article" />">Duis at ligula nec nisi interdum commodo sed vel felis. Aenean sit amet</a>
                            </h3>
                            <p>
                                21 minutes ago <a href="http://variety.com/">Variety</a>
                            </p>
                        </div>
                    </div>

                    <div class = "other-news-article-div">
                        <div class = "regular-news-article-div">
                            <h3>
                                <a href="<c:url value="/article" />">Duis at ligula nec nisi interdum commodo sed vel felis. Aenean sit amet</a>
                            </h3>
                            <p>
                                21 minutes ago <a href="http://variety.com/">Variety</a>
                            </p>
                        </div><!--

                        --><div class = "regular-news-article-div">
                        <h3>
                            <a href="<c:url value="/article" />">Praesent accumsan libero tincidunt dolor pulvinar porttitor. Morbi quis facilisis</a>
                        </h3>
                        <p>
                            21 minutes ago <a href="http://variety.com/">Variety</a>
                        </p>
                    </div>
                    </div>
                </div>

            </div>

            <div class = "box-office-div">
                <h2>
                    Top at the Box Office: <a title="See Who's Top 5 At The Box Office" href="<c:url value="/box-office" />">... see all</a>
                </h2>
                <div>
                    <div class = "sample-film-box-office">
                        <div>
                            <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/home/BoxOffice/inception.jpg" />"></a>
                        </div><!--
                        --><div>
                            <a href="<c:url value="/film" />">Inception</a>
                            <p>
                                C. Nolan
                            </p>
                            <p>
                                Gross: $43.0M
                            </p>
                        </div>
                    </div><!--

                    --><div class = "sample-film-box-office">
                        <div>
                            <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/home/BoxOffice/blackswan.jpg" />"></a>
                        </div><!--
                        --><div>
                            <a href="<c:url value="/film" />">Black Swan</a>
                            <p>
                                D. Aronofsky
                            </p>
                            <p>
                                Gross: $28.0M
                            </p>
                        </div>
                    </div><!--

                    --><div class = "sample-film-box-office">
                        <div>
                            <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/home/BoxOffice/sw.jpg" />"></a>
                        </div><!--
                            --><div>
                        <a href="<c:url value="/film" />">Star Wars 7</a>
                        <p>
                            J.J. Abrams
                        </p>
                        <p>
                            Gross: $17.0M
                        </p>
                    </div>
                </div>

                </div>
            </div>

            <!-- Content End -->

        </section>

        <footer>
            <span>Copyright &copy; 2016 - 2017 Dan-Ovidiu Graur</span>
        </footer>

    </body>
</html>
