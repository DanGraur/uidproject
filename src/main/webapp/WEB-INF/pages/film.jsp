<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<%--
  Created by IntelliJ IDEA.
  User: noi
  Date: 12/24/2016
  Time: 12:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<form name = "logoutForm" id = "logoutForm" action="<c:url value="/general/logout" />"></form>

<script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery-ui-1.12.1.custom/jquery-ui.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery.qtip.min.js" />"></script>
<script src="<c:url value="/resources/js/common.js" />?v=1.4"></script>

<div id="login-dialog" class = "pop-up-dialog" title="Please Log In First">
    <p>
        You must first <a href = "<c:url value="/login" />">Log In</a> to use this feature
    </p>
</div>


<div id="recommend-to-friend-dialog" class = "pop-up-dialog" title="Please Select a Friend">
    <label for = "friend-name-select">
        Who would you like to recommend this film to?
    </label>
    <select id = "friend-name-select" name = "friend-name-select">
        <c:forEach var = "o" items="${friends}">
            <option value="${o}">${o}</option>
        </c:forEach>
    </select>
    <button type="submit" name = "submit-to-friend-button" onclick="recommendFilm('sw')">Submit</button>
</div>

<div id="success-dialog" class = "pop-up-dialog" title="Success!">
    <p>
        <b>Success! Looks like everything went well!</b>
    </p>
</div>


<div id="error-dialog" class = "pop-up-dialog" title="Oops!">
    <p>
        <b>Oops! Looks like we've got some technical issues! Try again later...</b>
    </p>
</div>

<div id="buy-tickets-dialog" class = "pop-up-dialog" title="Ticket Menu">
    <p>
        <b>Please select your preferences:</b>
    </p>
    <table>
        <tr>
            <td><label for = "ticket-cinema-id">Cinema:</label></td>
            <td><select id = "ticket-cinema-id" onchange="changeDates()"></select></td>

            <td><label for = "ticket-date-id">Date:</label></td>
            <td><select id = "ticket-date-id" onchange="changeHours()"></select></td>

            <td><label for = "ticket-hour-id">Hour:</label></td>
            <td><select id = "ticket-hour-id" onchange="enableTicketButton()"></select></td>

            <td><label for = "ticket-count-id">Tickets:</label></td>
            <td>
                <select id = "ticket-count-id">
                    <c:forEach var="i" begin="1" end="10">
                        <option value="${i}">${i}</option>
                    </c:forEach>
                </select>
            </td>
            <td>
                <button type="submit" id = 'ticket-button' name = "options-selected-button" onclick="buyTickets()">Submit</button>
            </td>
        </tr>
    </table>
</div>

<div id="card-information-dialog" class = "pop-up-dialog" title="Card Information">
<p>
    <b>Please input your card information:</b>
</p>
    <table>
        <tr>
            <td><label for = "user-name">Full Name:</label></td>

            <td><input type = "text" id = "user-name" name = "user-name"></td>
        </tr>
        <tr>
            <td><label for = "card-number">Card Number:</label></td>

            <td><input type = "text" id = "card-number" name = "card-number"></td>
        </tr>
    </table>
    <button type="submit" id = 'card-info-button' name = "options-selected-button" onclick="submitCardInfo()">Submit</button>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

<!-- Attach Tooltips -->
$(document).ready(function()
{
    $('*[title]').each(function() {
        $(this).qtip({
            style: {
                classes: 'qtip-tipsy'
            }
        });
    });
});


function clearTextArea() {
    document.getElementsByName("review-input")[0].value = "";

    return false;
}

function confirmDelete() {
    return confirm("Are sure you want to delete you review? This cannot be undone!");
}

function checkReview() {
    var input = document.getElementsByName('review-input')[0].value;

    if (input == "") {
        alert('You cannot submit an empty review!');
        return false;
    }

    return true;
}

</script>

<head>
    <title>Star Wars: The Force Awakens</title>

    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/progress/minified/progressjs.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery.qtip.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery-ui-1.12.1.custom/jquery-ui.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/common.css" />?v=1.2" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/film.css" />?v=1.4" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/icon-effect.css" />" />
</head>
<body>
<header>
    <div>
        <a href="<c:url value="/home" />"><img src="<c:url value="/resources/images/common/logo.png" />"></a>
    </div><!--
            --><div>
    <nav>
        <ul>
            <li id = "notification-nav">
            </li>
            <li>
                <a title = "Go to Homepage"  href="<c:url value="/home" />">Home</a>
            </li>
            <c:choose>
                <c:when test = "${empty sessionScope.username}">
                    <li>
                        <a title = "Go to Login" href="<c:url value="/login" />">Login</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a title = "Go to My Account" href="<c:url value="/account" />">My Account</a>
                    </li>
                    <li>
                        <a title = "Logout" href="#" onclick="document.logoutForm.submit()" >Logout</a>
                    </li>
                </c:otherwise>
            </c:choose>
            <li>
                <a title = "Go to Help Page"  href="<c:url value="/help/actor" />">Help</a>
            </li>
        </ul>
    </nav>
</div>
</header>
<form method = "get">
    <table>
        <tr>
            <td id = "icon-table-cell">
                <img id = "magnifier-image" src="<c:url value="/resources/images/common/magnifier.png" />">
            </td>
            <td id = "search-table-cell">
                <input type="text" id = "search-input" autocomplete="off" placeholder="Search for a movie, actor, director..."/>
            </td>
        </tr>
    </table>
    <div id = "absolute-div">
        <ul id = "search-results">
        </ul>
    </div>
</form>
<section class = "body-content">

    <!-- Content Start -->
    <section class = "film-general-info">
        <div id = "film-poster">
            <img src="<c:url value="/resources/images/film/sw.jpg" />">
        </div><!--

        --><div id = "film-description">
            <h3>
                Start Wars: The Force Awakens <span>(2015)</span>
            </h3>
            <div id = "film-icons">
                <ul id = "general-icons">
                    <li>
                        <c:set var="contains" value="false" />

                        <c:forEach var="item" items="${sessionScope.films}">
                            <c:if test="${item == 'sw'}">
                                <c:set var="contains" value="true" />
                            </c:if>
                        </c:forEach>

                        <c:choose>
                            <c:when test="${contains == true}">
                                <img title = "Add/Remove Film To Watchlist" class = "focus-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="filmFavorite(this, 'sw')">
                            </c:when>
                            <c:otherwise>
                                <img title = "Add/Remove Film To Watchlist" class = "focus-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="filmFavorite(this, 'sw')">
                            </c:otherwise>
                        </c:choose>
                    </li>
                    <li><img title = "Buy Tickets" class = "focus-icon" src="<c:url value="/resources/images/icons/ticket.png" />" onclick="ticketOptionSelector()"></li>
                    <li><a href = "<c:url value="/slideshow" />"><img title = "See Pictures" class = "focus-icon" src="<c:url value="/resources/images/icons/review.png" />" onclick=""></a></li>
                    <li><a href = "#reviews"><img title = "Write Review" class = "focus-icon" src="<c:url value="/resources/images/icons/pics.png" />"></a></li>
                    <li><img title = "Recommend To A Friend" class = "focus-icon" src="<c:url value="/resources/images/icons/friend.png" />" onclick="recommendFilmPopUp()"></li>
                    <li><a href="https://twitter.com/intent/tweet" class="twitter-mention-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></li>
                    <li><div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="45" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div></li>
                </ul>
            </div>

            <div id = "film-details">
                <h3>
                    Duration:
                </h3>
                <p>
                    2h 16m
                </p>
                <h3>
                    Featured Crew:
                </h3>
                <div class = "featured-crew-div">
                    <p>
                        J.J. Abrams
                    </p>
                    <p>
                        Director/Screenplay
                    </p>
                </div><!--
                --><div class = "featured-crew-div">
                    <p>
                        Michael Arndt
                    </p>
                    <p>
                        Writer
                    </p>
                </div><!--
                --><div class = "featured-crew-div">
                    <p>
                        Lawrence Kasdan
                    </p>
                    <p>
                        Screenplay
                    </p>
                </div>
                <h3>
                    Plot:
                </h3>
                <p class = "justify-class">
                    Thirty years after the Galactic Empire vanquished, a new threat rises. The First Order attempt to rule the Galaxy and only a small bunch of heroes have the ability to stop them. Meanwhile, a female scavenger named Rey teams up with a rogue stormtroopter to return a droid to the resistance base containing a map to a legendary Jedi Master.
                </p>
            </div>
        </div>

        <div id = "stars-div">
            <h3>
                Stars:
            </h3>
            <div class = "film-star-div">
                <a href="<c:url value="/actor" />"><img src="<c:url value="/resources/images/film/Stars/daisy.jpg" />"></a>
                <p>
                    Daisy Ridley
                </p>
                <p>
                    Rey
                </p>
                <c:set var="contains" value="false" />

                <c:forEach var="item" items="${sessionScope.actors}">
                    <c:if test="${item == 'Daisy Ridley'}">
                        <c:set var="contains" value="true" />
                    </c:if>
                </c:forEach>

                <c:choose>
                    <c:when test="${contains == true}">
                        <img title = "Add/Remove Film To Favorites" class = "focus-icon cast-favorite-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="actorFavorite(this, 'Daisy Ridley')">
                    </c:when>
                    <c:otherwise>
                        <img title = "Add/Remove Actor To Favorites" class = "focus-icon cast-favorite-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="actorFavorite(this, 'Daisy Ridley')">
                    </c:otherwise>
                </c:choose>
            </div><!--
            --><div class = "film-star-div">
                <a href="<c:url value="/actor" />"><img src="<c:url value="/resources/images/film/Stars/daisy.jpg" />"></a>
                <p>
                    Harrison Ford
                </p>
                <p>
                    Han Solo
                </p>
                <c:set var="contains" value="false" />

                <c:forEach var="item" items="${sessionScope.actors}">
                    <c:if test="${item == 'Harrison Ford'}">
                        <c:set var="contains" value="true" />
                    </c:if>
                </c:forEach>

                <c:choose>
                    <c:when test="${contains == true}">
                        <img title = "Add/Remove Film To Favorites" class = "focus-icon cast-favorite-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="actorFavorite(this, 'Harrison Ford')">
                    </c:when>
                    <c:otherwise>
                        <img title = "Add/Remove Actor To Favorites" class = "focus-icon cast-favorite-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="actorFavorite(this, 'Harrison Ford')">
                    </c:otherwise>
                </c:choose>
            </div><!--
                --><div class = "film-star-div">
                <a href="<c:url value="/actor" />"><img src="<c:url value="/resources/images/film/Stars/daisy.jpg" />"></a>
                <p>
                    Mark Hamill
                </p>
                <p>
                    Luke Skywalker
                </p>
                <c:set var="contains" value="false" />

                <c:forEach var="item" items="${sessionScope.actors}">
                    <c:if test="${item == 'Mark Hamill'}">
                        <c:set var="contains" value="true" />
                    </c:if>
                </c:forEach>

                <c:choose>
                    <c:when test="${contains == true}">
                        <img title = "Add/Remove Film To Favorites" class = "focus-icon cast-favorite-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="actorFavorite(this, 'Mark Hamill')">
                    </c:when>
                    <c:otherwise>
                        <img title = "Add/Remove Actor To Favorites" class = "focus-icon cast-favorite-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="actorFavorite(this, 'Mark Hamill')">
                    </c:otherwise>
                </c:choose>
            </div><!--
                --><div class = "film-star-div">
                <a href="<c:url value="/actor" />"><img src="<c:url value="/resources/images/film/Stars/daisy.jpg" />"></a>
                <p>
                    Carrie Fisher
                </p>
                <p>
                    Princess Leia
                </p>
                <c:set var="contains" value="false" />

                <c:forEach var="item" items="${sessionScope.actors}">
                    <c:if test="${item == 'Carrie Fisher'}">
                        <c:set var="contains" value="true" />
                    </c:if>
                </c:forEach>

                <c:choose>
                    <c:when test="${contains == true}">
                        <img title = "Add/Remove Film To Favorites" class = "focus-icon cast-favorite-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="actorFavorite(this, 'Carrie Fisher')">
                    </c:when>
                    <c:otherwise>
                        <img title = "Add/Remove Actor To Favorites" class = "focus-icon cast-favorite-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="actorFavorite(this, 'Carrie Fisher')">
                    </c:otherwise>
                </c:choose>
            </div><!--
                --><div class = "film-star-div">
                <a href="<c:url value="/actor" />"><img src="<c:url value="/resources/images/film/Stars/daisy.jpg" />"></a>
                <p>
                    Adam Driver
                </p>
                <p>
                    Kylo Ren
                </p>
                <c:set var="contains" value="false" />

                <c:forEach var="item" items="${sessionScope.actors}">
                    <c:if test="${item == 'Adam Driver'}">
                        <c:set var="contains" value="true" />
                    </c:if>
                </c:forEach>

                <c:choose>
                    <c:when test="${contains == true}">
                        <img title = "Add/Remove Film To Favorites" class = "focus-icon cast-favorite-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="actorFavorite(this, 'Adam Driver')">
                    </c:when>
                    <c:otherwise>
                        <img title = "Add/Remove Actor To Favorites" class = "focus-icon cast-favorite-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="actorFavorite(this, 'Adam Driver')">
                    </c:otherwise>
                </c:choose>
            </div>
        </div>

        <div id = "multimedia-div">
            <h3>
                Trailer:
            </h3>
            <iframe width="853" height="480" class = "youtube-frame" src="https://www.youtube.com/embed/sGbxmsDFVnE" frameborder="0" allowfullscreen></iframe>
            <h3>
                Pictures:
            </h3>
            <a href="<c:url value="/slideshow" />"><img src="<c:url value="/resources/images/film/Stars/boyega.jpg" />"></a>
            <a href="<c:url value="/slideshow" />"><img src="<c:url value="/resources/images/film/RandomPics/1.jpg" />"></a>
            <a href="<c:url value="/slideshow" />"><img src="<c:url value="/resources/images/film/RandomPics/3.jpg" />"></a>
            <a href="<c:url value="/slideshow" />"><img src="<c:url value="/resources/images/film/RandomPics/4.jpg" />"></a>
            <a href="<c:url value="/slideshow" />"><img src="<c:url value="/resources/images/film/RandomPics/2.jpg" />"></a>
            <a href="<c:url value="/slideshow" />"><img src="<c:url value="/resources/images/film/RandomPics/5.jpg" />"></a>
            <a href="<c:url value="/slideshow" />"><img src="<c:url value="/resources/images/film/Stars/daisy.jpg" />"></a>
        </div>


    </section>

</section>
<section class = "detailed-movie-info">
    <h3>
        Details:
    </h3>
    <div class = "detail-row-div">
        <div class = "detail-element-div">
            <p>
                Status:
            </p>
            <p>
                Released
            </p>
        </div><!--
        --><div class = "detail-element-div">
            <p>
                Film Length:
            </p>
            <p>
                2h 16m
            </p>
        </div><!--
            --><div class = "detail-element-div">
            <p>
                Revenue:
            </p>
            <p>
                $2,068,178,225
            </p>
        </div><!--
            --><div class = "detail-element-div">
            <p>
                Release Information:
            </p>
            <p>
                December 18, 2015
            </p>
        </div>
    </div>

    <div class = "detail-row-div">
        <div class = "detail-element-div">
            <p>
                Film Language:
            </p>
            <p>
                English
            </p>
        </div><!--
        --><div class = "detail-element-div">
            <p>
                Budget:
            </p>
            <p>
                $200,000,000.00
            </p>
        </div><!--
                --><div class = "detail-element-div">
            <p>
                Homepage:
            </p>
            <p>
                <a href="http://starwars.com">http://starwars.com</a>
            </p>
        </div><!--
                --><div class = "detail-element-div">
            <p>
                Rating:
            </p>
            <p>
                PG-13
            </p>
        </div>
    </div>
</section>

<section class = "body-content">
    <a name="reviews"></a>
    <section class="film-general-info">
        <div id = "reviews-div">
            <h3>
                Reviews:
            </h3>

            <c:choose>
                <c:when test = "${empty sessionScope.username}">
                    <c:forEach var="o" items="${reviews}">
                        <div class = "review-div">
                            <p>
                                <span>A review by ${o.username}:</span>
                                ${o.review}
                            </p>
                        </div>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <c:forEach var="o" items="${reviews}">
                        <c:if test="${o.userReview != true}">
                            <div class = "review-div">
                                <p>
                                    <span>A review by ${o.username}:</span>
                                    ${o.review}
                                </p>
                            </div>
                        </c:if>
                    </c:forEach>
                    <c:if test="${sessionScope.hasReview == false}">
                        <div class="user-review-div">
                            <form name="review-form" method = "post">
                                <textarea rows="5" name = "review-input" cols="40" placeholder="Input your review here..."></textarea>
                                <input title = "Post Your Review" type="submit" id="submit-review-button" name = "submit-review-button" value="Submit" onclick="return checkReview()"/>
                                <button title = "Clear The Review" onclick="return clearTextArea()" id = "clear-review-button">Reset</button>
                            </form>
                        </div>
                    </c:if>
                    <c:if test="${not empty sessionScope.hasReview}">
                        <c:forEach var="o" items="${reviews}">
                            <c:if test="${o.userReview == true}">
                                <div class = "review-div">
                                    <p>
                                        <span>A review by ${o.username}:</span>
                                        ${o.review}
                                    </p>

                                    <form name="delete-review-form" method = "post">
                                        <input title = "Delete Your Review" type="submit" id="delete-review-button" name = "delete-review-button" value="Delete" onclick="return confirmDelete()"/>
                                    </form>
                                </div>
                            </c:if>
                        </c:forEach>
                    </c:if>
                </c:otherwise>
            </c:choose>

        </div>
    </section>
</section>

<!-- End of content -->

<footer>
    <span>Copyright &copy; 2016 - 2017 Dan-Ovidiu Graur</span>
</footer>

</body>
</html>
