<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<%--
  Created by IntelliJ IDEA.
  User: noi
  Date: 12/24/2016
  Time: 12:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<form name = "logoutForm" id = "logoutForm" action="<c:url value="/general/logout" />"></form>

<script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery-ui-1.12.1.custom/jquery-ui.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery.qtip.min.js" />"></script>
<script src="<c:url value="/resources/js/common.js" />?v=1.2"></script>

<div id="login-dialog" class = "pop-up-dialog" title="Please Log In First">
    <p>
        You must first <a href = "<c:url value="/login" />">Log In</a> to use this feature
    </p>
</div>

<div id="success-dialog" class = "pop-up-dialog" title="Success!">
    <p>
        <b>Success! Looks like everything went well!</b>
    </p>
</div>

<div id="error-dialog" class = "pop-up-dialog" title="Oops!">
    <p>
        <b>Oops! Looks like we've got some technical issues! Try again later...</b>
    </p>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


<!-- Attach Tooltips -->
$(document).ready(function()
{
    $('*[title]').each(function() {
        $(this).qtip({
            style: {
                classes: 'qtip-tipsy'
            }
        });
    });
});

</script>

<head>
    <title>Benedict Cumberbatch</title>

    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/progress/minified/progressjs.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery.qtip.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery-ui-1.12.1.custom/jquery-ui.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/common.css" />?v=1.1" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/actor.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/icon-effect.css" />" />
</head>
<body>
<header>
    <div>
        <a href="<c:url value="/home" />"><img src="<c:url value="/resources/images/common/logo.png" />"></a>
    </div><!--
            --><div>
    <nav>
        <ul>
            <li id = "notification-nav">
            </li>
            <li>
                <a title = "Go to Homepage" href="<c:url value="/home" />">Home</a>
            </li>
            <c:choose>
                <c:when test = "${empty sessionScope.username}">
                    <li>
                        <a title = "Go to Login" href="<c:url value="/login" />">Login</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a title = "Go to My Account" href="<c:url value="/account" />">My Account</a>
                    </li>
                    <li>
                        <a title = "Logout" href="#" onclick="document.logoutForm.submit()" >Logout</a>
                    </li>
                </c:otherwise>
            </c:choose>
            <li>
                <a title = "Go to Help Page" href="<c:url value="/help/actor" />">Help</a>
            </li>
        </ul>
    </nav>
</div>
</header>
<form method = "get">
    <table>
        <tr>
            <td id = "icon-table-cell">
                <img id = "magnifier-image" src="<c:url value="/resources/images/common/magnifier.png" />">
            </td>
            <td id = "search-table-cell">
                <input type="text" id = "search-input" autocomplete="off" placeholder="Search for a movie, actor, director..."/>
            </td>
        </tr>
    </table>
    <div id = "absolute-div">
        <ul id = "search-results">
        </ul>
    </div>
</form>
<section class = "body-content">

    <!-- Content Start -->
    <section class = "actor-general-info">
        <div id = "actor-poster">
            <img src="<c:url value="/resources/images/actor/benedict.jpg" />">
        </div><!--

        --><div id = "actor-description">
        <h3>
            Benedict Cumberbatch
        </h3>
        <div id = "actor-icons">
            <ul id = "general-icons">
                <li>
                    <c:set var="contains" value="false" />

                    <c:forEach var="item" items="${sessionScope.actors}">
                        <c:if test="${item == 'Benedict Cumberbatch'}">
                            <c:set var="contains" value="true" />
                        </c:if>
                    </c:forEach>

                    <c:choose>
                        <c:when test="${contains == true}">
                            <img title = "Add/Remove Film To Favorites" class = "focus-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="actorFavorite(this, 'Benedict Cumberbatch')">
                        </c:when>
                        <c:otherwise>
                            <img title = "Add/Remove Actor To Favorites" class = "focus-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="actorFavorite(this, 'Benedict Cumberbatch')">
                        </c:otherwise>
                    </c:choose>
                </li>

                <li><a href="https://twitter.com/intent/tweet" class="twitter-mention-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></li>
                <li><div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="45" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div></li>
            </ul>
        </div>

        <div id = "actor-details">
            <div class = "further-actor-details-div">
                <h3>
                    Birthday:
                </h3>
                <p>
                    1976-07-19
                </p>
            </div><!--
            --><div class = "further-actor-details-div">
            <h3>
                Place of Birth:
            </h3>
            <p>
                London, England
            </p>
        </div><!--
            --><div class = "further-actor-details-div">
            <h3>
                Gender:
            </h3>
            <p>
                Male
            </p>
        </div><!--
            --><div class = "further-actor-details-div">
            <h3>
                Credits:
            </h3>
            <p>
                84
            </p>
        </div>

            <h3>
                Biography:
            </h3>
            <p class = "justify-class">
                Benedict Timothy Carlton Cumberbatch was born and raised in London, England. His parents, Wanda Ventham and Timothy Carlton (Timothy Carlton Congdon Cumberbatch), are both actors. He is a grandson of submarine commander Henry Carlton Cumberbatch, and a great-grandson of diplomat Henry Arnold Cumberbatch CMG. Cumberbatch attended Brambletye School and Harrow School. Whilst at Harrow, he had an arts scholarship and painted large oil canvases. It's also where he began acting. After he finished school, he took a year off to volunteer as an English teacher in a Tibetan monastery in Darjeeling, India. On his return, he studied drama at Manchester University. He continued his training as an actor at the...
            </p>
        </div>
    </div>

        <div id = "actor-known-for-div">
            <h3>
                Known For:
            </h3>
            <div class = "actor-film-div">
                <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/actor/Films/hobbit.jpg" />"></a>
                <p>
                    The Hobbit: An Unexpected Journey
                </p>
                <p>
                    2012
                </p>
            </div><!--
            --><div class = "actor-film-div">
                <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/actor/Films/st.jpg" />"></a>
                <p>
                    Start Trek: Into The Darkness
                </p>
                <p>
                    2012
                </p>
            </div><!--
            --><div class = "actor-film-div">
                <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/actor/Films/imitation-game.jpg" />"></a>
                <p>
                    The Imitation Game
                </p>
                <p>
                    2014
                </p>
            </div><!--
            --><div class = "actor-film-div">
                <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/actor/Films/12years.jpg" />"></a>
                <p>
                    12 Years A Slave
                </p>
                <p>
                    2013
                </p>
            </div><!--
            --><div class = "actor-film-div">
                <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/actor/Films/sherlock.jpg" />"></a>
                <p>
                    Sherlock
                </p>
                <p>
                    2010
                </p>
            </div><!--
            --><div class = "actor-film-div">
                <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/actor/Films/war-horse.jpg" />"></a>
                <p>
                    War Horse
                </p>
                <p>
                    2011
                </p>
            </div>
        </div>
    </section>
</section>

<section class = "detailed-actor-info">
    <h3>
        Acting Roles:
    </h3>
    <ul>
        <li><span>2018&nbsp;&nbsp;</span><img src="<c:url value="/resources/images/icons/circle.png" />">&nbsp;&nbsp;<a href="<c:url value="/film" />">How the Grinch Stole Christmas!&nbsp;</a>&nbsp;as Grinch (Voice) </li>
        <li><span>2018&nbsp;&nbsp;</span><img src="<c:url value="/resources/images/icons/circle.png" />">&nbsp;&nbsp;<a href="<c:url value="/film" />">Jungle Book&nbsp;</a>&nbsp;as Shere Khan </li>
        <li><span>2018&nbsp;&nbsp;</span><img src="<c:url value="/resources/images/icons/circle.png" />">&nbsp;&nbsp;<a href="<c:url value="/film" />">Avengers: Infinity War&nbsp;</a>&nbsp;as Stephen Strange / Doctor Strange </li>
        <li><span>2017&nbsp;&nbsp;</span><img src="<c:url value="/resources/images/icons/circle.png" />">&nbsp;&nbsp;<a href="<c:url value="/film" />">Flying Horse&nbsp;</a>&nbsp;as Harry Larkyns </li>
        <li><span>2017&nbsp;&nbsp;</span><img src="<c:url value="/resources/images/icons/circle.png" />">&nbsp;&nbsp;<a href="<c:url value="/film" />">Thor: Ragnarok&nbsp;</a>&nbsp;as Stephen Strange / Doctor Strange </li>
        <li><span>2016&nbsp;&nbsp;</span><img src="<c:url value="/resources/images/icons/circle.png" />">&nbsp;&nbsp;<a href="<c:url value="/film" />">Magik&nbsp;</a>&nbsp;as Lewis </li>
        <li><span>2016&nbsp;&nbsp;</span><img src="<c:url value="/resources/images/icons/circle.png" />">&nbsp;&nbsp;<a href="<c:url value="/film" />">Doctor Strange&nbsp;</a>&nbsp;as Stephen Strange / Doctor Strange </li>
        <li><span>2016&nbsp;&nbsp;</span><img src="<c:url value="/resources/images/icons/circle.png" />">&nbsp;&nbsp;<a href="<c:url value="/film" />">Naples '44&nbsp;</a>&nbsp;as Norman Lewis </li>
    </ul>
</section>


<section class = "twitter-timeline">
    <h3>
        Twitter Timeline:
    </h3>
    <a class="twitter-timeline" data-height="900" href="https://twitter.com/LeoDiCaprio">Tweets by LeoDiCaprio</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
</section>

<!-- End of Content -->

<footer>
    <span>Copyright &copy; 2016 - 2017 Dan-Ovidiu Graur</span>
</footer>

</body>
</html>
