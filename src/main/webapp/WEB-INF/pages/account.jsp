<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<%--
  Created by IntelliJ IDEA.
  User: noi
  Date: 12/24/2016
  Time: 12:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<form name = "logoutForm" id = "logoutForm" action="<c:url value="/general/logout" />"></form>

<script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery-ui-1.12.1.custom/jquery-ui.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery.qtip.min.js" />"></script>
<script src="<c:url value="/resources/js/common.js" />"></script>

<div id="login-dialog" class = "pop-up-dialog" title="Please Log In First">
    <p>
        You must first <a href = "<c:url value="/login" />">Log In</a> to use this feature
    </p>
</div>

<div id="success-dialog" class = "pop-up-dialog" title="Success!">
    <p>
        <b>Success! Looks like everything went well!</b>
    </p>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


<!-- Attach Tooltips -->
$(document).ready(function()
{
    $('*[title]').each(function() {
        $(this).qtip({
            style: {
                classes: 'qtip-tipsy'
            }
        });
    });
});


function clearTextArea() {
    document.getElementsByName("comment-input")[0].value = "";

    return false;
}


function checkComment() {
    var input = document.getElementsByName('comment-input')[0].value;

    if (input == "") {
        alert('You cannot submit an empty comment!');
        return false;
    }

    return true;
}

</script>

<head>
    <title>Account</title>

    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/progress/minified/progressjs.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery.qtip.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery-ui-1.12.1.custom/jquery-ui.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/common.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/article.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/icon-effect.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/account.css" />?v=1.1" />

    <meta http-equiv="refresh" content="5">
</head>
<body>
<header>
    <div>
        <a href="<c:url value="/home" />"><img src="<c:url value="/resources/images/common/logo.png" />"></a>
    </div><!--
            --><div>
    <nav>
        <ul>
            <li id = "notification-nav">
            </li>
            <li>
                <a title = "Go to Homepage" href="<c:url value="/home" />">Home</a>
            </li>
            <c:choose>
                <c:when test = "${empty sessionScope.username}">
                    <li>
                        <a title = "Go to Login" href="<c:url value="/login" />">Login</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a title = "Go to My Account" href="<c:url value="/account" />">My Account</a>
                    </li>
                    <li>
                        <a title = "Logout" href="#" onclick="document.logoutForm.submit()" >Logout</a>
                    </li>
                </c:otherwise>
            </c:choose>
            <li>
                <a title = "Go to Help Page" href="<c:url value="/help/general" />">Help</a>
            </li>
        </ul>
    </nav>
</div>
</header>
<form method = "get">
    <table>
        <tr>
            <td id = "icon-table-cell">
                <img id = "magnifier-image" src="<c:url value="/resources/images/common/magnifier.png" />">
            </td>
            <td id = "search-table-cell">
                <input type="text" id = "search-input" autocomplete="off" placeholder="Search for a movie, actor, director..."/>
            </td>
        </tr>
    </table>
    <div id = "absolute-div">
        <ul id = "search-results">
        </ul>
    </div>
</form>

<!-- Content Start -->

<section>

    <div class = "image-collection-div">

        <h2>Favorite Images:</h2>

        <c:forEach var = "i" items="${sessionScope.images}">

            <img src="<c:url value="${i}" />">

        </c:forEach>

    </div>

    <div class="clear-fix"></div>

    <div class = "common-div">

        <h2>Favorite Actors:</h2>

        <c:forEach var = "i" items="${sessionScope.actors}">

            <div class = "film-star-div">
                <a href="<c:url value="/actor" />"><img src="<c:url value="/resources/images/film/Stars/daisy.jpg" />"></a>
                <p>
                    ${i}
                </p>
            </div>

    </c:forEach>

    </div>

    <div class="clear-fix"></div>

    <div class = "common-div">

        <h2>Favorite Films:</h2>

        <c:forEach var = "i" items="${actualFilms}">

            <div class = "film-star-div">
                <a href="<c:url value="/film" />"><img src="<c:url value="/resources/images/home/BoxOffice/sw.jpg" />"></a>
                <p>
                        ${i}
                </p>
            </div>

        </c:forEach>

    </div>

    <div class="clear-fix"></div>

</section>

<!-- Content end -->

<footer>
    <span>Copyright &copy; 2016 - 2017 Dan-Ovidiu Graur</span>
</footer>

</body>
</html>
