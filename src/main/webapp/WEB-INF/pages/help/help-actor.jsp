<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<%--
  Created by IntelliJ IDEA.
  User: noi
  Date: 12/24/2016
  Time: 12:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<form name = "logoutForm" id = "logoutForm" action="<c:url value="/general/logout" />"></form>

<script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery.qtip.min.js" />"></script>
<script src="<c:url value="/resources/js/common.js" />"></script>

<script>

    <!-- Attach Tooltips -->
    $(document).ready(function()
    {
        $('*[title]').each(function() {
            $(this).qtip({
                style: {
                    classes: 'qtip-tipsy'
                }
            });
        });
    });

</script>


<head>
    <title>Actor Page Help</title>

    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/progress/minified/progressjs.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery.qtip.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/common.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/help.css" />" />
</head>
<body>
<header>
    <div>
        <a href="<c:url value="/home" />"><img src="<c:url value="/resources/images/common/logo.png" />"></a>
    </div><!--
            --><div>
    <nav>
        <ul>
            <li id = "notification-nav">
            </li>
            <li>
                <a title = "Go to Homepage" href="<c:url value="/home" />">Home</a>
            </li>
            <c:choose>
                <c:when test = "${empty sessionScope.username}">
                    <li>
                        <a title = "Go to Login" href="<c:url value="/login" />">Login</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a title = "Go to My Account" href="<c:url value="/account" />">My Account</a>
                    </li>
                    <li>
                        <a title = "Logout" href="#" onclick="document.logoutForm.submit()" >Logout</a>
                    </li>
                </c:otherwise>
            </c:choose>
            <li>
                <a title = "Go to Help Page" href="<c:url value="/help/general" />">Help</a>
            </li>
        </ul>
    </nav>
</div>
</header>
<form method = "get">
    <table>
        <tr>
            <td id = "icon-table-cell">
                <img id = "magnifier-image" src="<c:url value="/resources/images/common/magnifier.png" />">
            </td>
            <td id = "search-table-cell">
                <input type="text" id = "search-input" autocomplete="off" placeholder="Search for a movie, actor, director..."/>
            </td>
        </tr>
    </table>
    <div id = "absolute-div">
        <ul id = "search-results">
        </ul>
    </div>
</form>
<section class = "body-content">

    <!-- Content Start -->

    <div>
        <h2>
            Actor Page F.A.Q.:
        </h2>

        <ul class = "help-list">
            <li><a href="<c:url value="login" />">How Can I Log In?</a></li>
            <li><a href="<c:url value="actor" />">Actor Page Help</a></li>
            <li><a href="<c:url value="film" />">Film Page Help</a></li>
            <li><a href="<c:url value="article" />">Article Page Help</a></li>
            <li><a href="<c:url value="slideshow" />">Slideshow Page Help</a></li>
            <li><a href="<c:url value="box-office" />">Box-Office Page Help</a></li>
            <li><a href="<c:url value="home" />">Home Page Help</a></li>
        </ul>
    </div>

    <!-- Content End -->

</section>

<footer>
    <span>Copyright &copy; 2016 - 2017 Dan-Ovidiu Graur</span>
</footer>

</body>
</html>
