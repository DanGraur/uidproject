<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<%--
  Created by IntelliJ IDEA.
  User: noi
  Date: 12/24/2016
  Time: 12:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery.qtip.min.js" />"></script>
<script src="<c:url value="/resources/js/common.js" />"></script>

<script>

    <!-- Attach Tooltips -->
    $(document).ready(function()
    {
        $('*[title]').each(function() {
            $(this).qtip({
                style: {
                    classes: 'qtip-tipsy'
                }
            });
        });
    });

</script>


<head>
    <title>Login</title>

    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/progress/minified/progressjs.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery.qtip.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/common.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/login.css" />?v=1.1" />
</head>
<body>
<header>
    <div>
        <a href="<c:url value="/home" />"><img src="<c:url value="/resources/images/common/logo.png" />"></a>
    </div><!--
            --><div>
    <nav>
        <ul>
            <li>
                <a title = "Go to Homepage" href="<c:url value="/home" />">Home</a>
            </li>
            <li>
                <a title = "Go to Help Page" href="<c:url value="/help/login" />">Help</a>
            </li>
        </ul>
    </nav>
</div>
</header>
<form method = "get">
    <table>
        <tr>
            <td id = "icon-table-cell">
                <img id = "magnifier-image" src="<c:url value="/resources/images/common/magnifier.png" />">
            </td>
            <td id = "search-table-cell">
                <input type="text" id = "search-input" autocomplete="off" placeholder="Search for a movie, actor, director..."/>
            </td>
        </tr>
    </table>
    <div id = "absolute-div">
        <ul id = "search-results">
        </ul>
    </div>
</form>
<section class = "body-content">

    <!-- Content Start -->
    <div>
        <h2>
            Log Into Your Account:
        </h2>

        <form name = "login-form" class = "body-form" method="post">
            <label>
                Username (case sensitive):
            </label>
            <input type = "text" id = "username" name = "username" placeholder="Input Username Here...">
            <label>
                Password (case sensitive):
            </label>
            <input type = "password" id = "password" name = "password"  placeholder="Input Password Here...">
            <input title = "Press To Proceed With Authentication" type = "submit" name = "login-button" id = "login-button" value="Log In" />
        </form>
        <p>
            <span style = "color: red; text-align: center;">${errMsg}</span>
        </p>
        <p>
            Don't have an account? <a href="<c:url value="/home" />">Sign Up</a>
        </p>
    </div>

    <!-- Content End -->

</section>

<footer>
    <span>Copyright &copy; 2016 - 2017 Dan-Ovidiu Graur</span>
</footer>

</body>
</html>
