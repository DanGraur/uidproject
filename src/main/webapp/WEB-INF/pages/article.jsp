<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<%--
  Created by IntelliJ IDEA.
  User: noi
  Date: 12/24/2016
  Time: 12:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<form name = "logoutForm" id = "logoutForm" action="<c:url value="/general/logout" />"></form>

<script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery-ui-1.12.1.custom/jquery-ui.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery.qtip.min.js" />"></script>
<script src="<c:url value="/resources/js/common.js" />?v=1.0"></script>

<div id="login-dialog" class = "pop-up-dialog" title="Please Log In First">
    <p>
        You must first <a href = "<c:url value="/login" />">Log In</a> to use this feature
    </p>
</div>

<div id="success-dialog" class = "pop-up-dialog" title="Success!">
    <p>
        <b>Success! Looks like everything went well!</b>
    </p>
</div>

<div id="error-dialog" class = "pop-up-dialog" title="Oops!">
    <p>
        <b>Oops! Looks like we've got some technical issues! Try again later...</b>
    </p>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


<!-- Attach Tooltips -->
$(document).ready(function()
{
    $('*[title]').each(function() {
        $(this).qtip({
            style: {
                classes: 'qtip-tipsy'
            }
        });
    });
});


function clearTextArea() {
    document.getElementsByName("comment-input")[0].value = "";

    return false;
}


function checkComment() {
    var input = document.getElementsByName('comment-input')[0].value;

    if (input == "") {
        alert('You cannot submit an empty comment!');
        return false;
    }

    // Ajax request

    // Post comment internally
    $.get("/article/comment?comment-input=" + input);

    $.get("/general/username", function (username) {
        var commentDiv = $(".user-comment-div")[0];

        // remove the comment div temporarily
        commentDiv.remove();

        var $aDiv = $("<div>");
        $aDiv.addClass("comment-div");

        var $aPara = $("<p>");

        var $aSpan = $("<span>");

        $aPara.append($aSpan);
        $aDiv.append($aPara);

        $aSpan.text("A comment by " + ((username == "") ? "Anonymous" : username));
        $aPara.append(input);


        $("#comment-div")
            .append($aDiv)
            .append(commentDiv);

        clearTextArea();
    });

    return false;
}

</script>

<head>
    <title>Article</title>

    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/progress/minified/progressjs.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery.qtip.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery-ui-1.12.1.custom/jquery-ui.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/common.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/article.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/icon-effect.css" />?v=1.5" />
</head>
<body>
<header>
    <div>
        <a href="<c:url value="/home" />"><img src="<c:url value="/resources/images/common/logo.png" />"></a>
    </div><!--
            --><div>
    <nav>
        <ul>
            <li id = "notification-nav">
            </li>
            <li>
                <a title = "Go to Homepage" href="<c:url value="/home" />">Home</a>
            </li>
            <c:choose>
                <c:when test = "${empty sessionScope.username}">
                    <li>
                        <a title = "Go to Login" href="<c:url value="/login" />">Login</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a title = "Go to My Account" href="<c:url value="/account" />">My Account</a>
                    </li>
                    <li>
                        <a title = "Logout" href="#" onclick="document.logoutForm.submit()" >Logout</a>
                    </li>
                </c:otherwise>
            </c:choose>
            <li>
                <a title = "Go to Help Page" href="<c:url value="/help/general" />">Help</a>
            </li>
        </ul>
    </nav>
</div>
</header>
<form method = "get">
    <table>
        <tr>
            <td id = "icon-table-cell">
                <img id = "magnifier-image" src="<c:url value="/resources/images/common/magnifier.png" />">
            </td>
            <td id = "search-table-cell">
                <input type="text" id = "search-input" autocomplete="off" placeholder="Search for a movie, actor, director..."/>
            </td>
        </tr>
    </table>
    <div id = "absolute-div">
        <ul id = "search-results">
        </ul>
    </div>
</form>

<section class = "article-content">

    <!-- Content Start -->
    <section class = "article-details">
        <img align="left" src="<c:url value="/resources/images/film/Stars/daisy.jpg" />">

        <span id = "title-span">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span><br>

        <span id = "detail-span">21 minutes ago <a href="http://variety.com/">Variety</a></span><br>

        <ul id = "general-icons">
            <li><a href = "#comments"><img title = "Write Comment" class = "focus-icon" style="height: 45px; width: 45px;" src="<c:url value="/resources/images/icons/pics.png" />"></a></li>
            <li><a href="https://twitter.com/intent/tweet" class="twitter-mention-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></li>
            <li><div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="45" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div></li>
        </ul>

        <span id = "article-span">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mattis rhoncus lorem vitae tempus. Etiam consectetur massa sed augue lacinia, eu fringilla nunc finibus. Phasellus pellentesque quam vitae turpis porttitor accumsan. Phasellus et gravida ante, ac tincidunt leo. Mauris interdum velit vitae nisl finibus cursus. Phasellus eu urna tempor, aliquet lacus efficitur, molestie mi. Nunc velit leo, porttitor sit amet porttitor id, molestie non odio. Nam iaculis molestie lorem, laoreet mattis augue ultricies eget. Vestibulum ultrices est sem, eget vehicula velit eleifend et.

            Aliquam auctor orci eget nunc cursus, et convallis nunc convallis. Proin et varius lectus. Suspendisse potenti. Proin vel euismod purus, in varius ipsum. Duis ac purus eros. Nunc ut ullamcorper purus. In nec dapibus mauris. Pellentesque aliquet metus ac placerat ullamcorper. Ut commodo dui in libero venenatis, ut porta odio placerat. In elementum ullamcorper nisl.

            Nulla ut posuere enim. Vivamus eu pharetra dolor, sit amet euismod nibh. Integer sed turpis eget dui semper volutpat. Aliquam porta, metus euismod congue dapibus, dui lorem congue tellus, nec rutrum purus nulla et felis. Donec eleifend nec ipsum in sodales. Phasellus tempor purus non lacus blandit, nec faucibus ligula scelerisque. Proin nec ipsum sit amet sapien commodo maximus. Nunc ullamcorper vel diam at scelerisque. Nullam ut ex eget ex fermentum molestie. Fusce nunc massa, consectetur a viverra quis, commodo eu lectus. Etiam non justo in ex egestas facilisis. Ut laoreet urna et nibh molestie convallis in ut nisl. Pellentesque finibus consectetur massa, ac molestie urna. Morbi finibus nisi at ornare malesuada. Morbi volutpat laoreet odio id placerat. Aliquam fermentum placerat velit, ac eleifend lorem.
        </span>
    </section>
</section>
<section class = "detailed-article-info">
    <h3>
        Involved in the Story:
    </h3>
    <div class = "detail-element-div">
        <a href="<c:url value="/actor" />"><img src="<c:url value="/resources/images/actor/benedict.jpg" />"></a>
        <p>
            Benedict Cumberbatch
        </p>
        <c:set var="contains" value="false" />

        <c:forEach var="item" items="${sessionScope.actors}">
            <c:if test="${item == 'Benedict Cumberbatch'}">
                <c:set var="contains" value="true" />
            </c:if>
        </c:forEach>

        <c:choose>
            <c:when test="${contains == true}">
                <img title = "Add/Remove Film To Favorites" class = "focus-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="actorFavorite(this, 'Benedict Cumberbatch')">
            </c:when>
            <c:otherwise>
                <img title = "Add/Remove Actor To Favorites" class = "focus-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="actorFavorite(this, 'Benedict Cumberbatch')">
            </c:otherwise>
        </c:choose>
    </div><!--
    --><div class = "detail-element-div">
        <a href="<c:url value="/actor" />"><img src="<c:url value="/resources/images/film/Stars/boyega.jpg" />"></a>
        <p>
            John Boyega
        </p>
        <c:set var="contains" value="false" />

        <c:forEach var="item" items="${sessionScope.actors}">
            <c:if test="${item == 'John Boyega'}">
                <c:set var="contains" value="true" />
            </c:if>
        </c:forEach>

        <c:choose>
            <c:when test="${contains == true}">
                <img title = "Add/Remove Film To Favorites" class = "focus-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="actorFavorite(this, 'John Boyega')">
            </c:when>
            <c:otherwise>
                <img title = "Add/Remove Actor To Favorites" class = "focus-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="actorFavorite(this, 'John Boyega')">
            </c:otherwise>
        </c:choose>
    </div><!--
    --><div class = "detail-element-div">
        <a href="<c:url value="/actor" />"><img src="<c:url value="/resources/images/film/Stars/daisy.jpg" />"></a>
        <p>
            Daisy Ridley
        </p>
        <c:set var="contains" value="false" />

        <c:forEach var="item" items="${sessionScope.actors}">
            <c:if test="${item == 'Daisy Ridley'}">
                <c:set var="contains" value="true" />
            </c:if>
        </c:forEach>

        <c:choose>
            <c:when test="${contains == true}">
                <img title = "Add/Remove Film To Favorites" class = "focus-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="actorFavorite(this, 'Daisy Ridley')">
            </c:when>
            <c:otherwise>
                <img title = "Add/Remove Actor To Favorites" class = "focus-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="actorFavorite(this, 'Daisy Ridley')">
            </c:otherwise>
        </c:choose>
    </div>
</section>

<section class = "article-content">
    <div id = "comment-div">
        <h3>
            Comments:
        </h3>

        <c:forEach var="o" items="${comments}">
            <div class = "comment-div">
                <p>
                    <span>A comment by ${o.username}:</span>
                    ${o.content}
                </p>
            </div>
        </c:forEach>
        <div class="user-comment-div">
            <form name="comment-form" method = "post">
                <textarea rows="5"  cols="40" name = "comment-input" placeholder="Input your comment here..."></textarea>
                <input title = "Post Your Comment" type="submit" id="submit-comment-button" name = "submit-comment-button" value="Submit" onclick="return checkComment()"/>
                <button title = "Clear The Comment" onclick="return clearTextArea()" id = "clear-comment-button">Reset</button>
            </form>
        </div>
    </div>
</section>
<a name="comments"></a>

<footer>
    <span>Copyright &copy; 2016 - 2017 Dan-Ovidiu Graur</span>
</footer>

</body>
</html>
