<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: noi
  Date: 12/24/2016
  Time: 12:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Homepage</title>

    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/common.css" />" />
</head>
<body>
<header>
    <div>
        <a href="<c:url value="/home" />"><img src="<c:url value="/resources/images/common/logo.png" />"></a>
    </div><!--
            --><div>
    <nav>
        <ul>
            <li>
                <a href="<c:url value="/home" />">Home</a>
            </li>
            <li>
                <a href="<c:url value="/login" />">Login</a>
            </li>
            <li>
                <a href="<c:url value="/help/general" />">Help</a>
            </li>
        </ul>
    </nav>
</div>
</header>
<form method = "get">
    <table>
        <tr>
            <td id = "icon-table-cell">
                <img id = "magnifier-image" src="<c:url value="/resources/images/common/magnifier.png" />">
            </td>
            <td id = "search-table-cell">
                <input type="text" placeholder="Search for a movie, actor, director..."/>
            </td>
        </tr>
    </table>
</form>
<section class = "body-content">

    <!-- Content Start -->
    <div>
        <h2>
            Recent Movies:
        </h2>
    </div>


    <!-- Content End -->

</section>

<footer>
    <span>Copyright &copy; 2016 - 2017 Dan-Ovidiu Graur</span>
</footer>

</body>
</html>
