<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<%--
  Created by IntelliJ IDEA.
  User: noi
  Date: 12/24/2016
  Time: 12:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<form name = "logoutForm" id = "logoutForm" action="<c:url value="/general/logout" />"></form>

<script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery.qtip.min.js" />"></script>
<script src="<c:url value="/resources/js/common.js" />?v=1.0"></script>

<div id="error-dialog" class = "pop-up-dialog" title="Oops!">
    <p>
        <b>Oops! Looks like we've got some technical issues! Try again later...</b>
    </p>
</div>

<script>
    var slideIndex = 1;

    function plusDivs(n) {
        showDivs(slideIndex += n);
    }

    function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("mySlides");
        var y = document.getElementsByClassName("focus-icon");
        if (n > x.length) {slideIndex = 1}
        if (n < 1) {slideIndex = x.length}
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
            try {
                y[i].style.display = "none";
            } catch (err) {

            }
        }
        x[slideIndex-1].style.display = "block";
        try {
            y[slideIndex - 1].style.display = "block";
        } catch (err) {

        }
    }

    function imageFavorite(element, imagePath) {
        var src = $(element).attr('src').split("/");

        $.ajax({
            url: "/slideshow/favorite",
            data: {path: imagePath},
            success: function() {
                if (src[src.length - 1] == 'wishlist.png')
                    $(element).attr('src', '/resources/images/icons/correct.png');
                else
                    $(element).attr('src', '/resources/images/icons/wishlist.png');
            },
            error: function () {
                $("#error-dialog").dialog({
                    position: {
                        my: "top",
                        at: "top",
                        of: window
                    },
                    width: 315
                });
            }
        });
    }

    $(document).ready(function() {
        showDivs(1);

        $('*[title]').each(function() {
            $(this).qtip({
                style: {
                    classes: 'qtip-tipsy'
                }
            });
        });
    });

</script>


<head>
    <title>Homepage</title>

    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/progress/minified/progressjs.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/js/jquery.qtip.min.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/w3.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/common.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/slideshow.css" />" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/icon-effect.css" />" />
</head>
<body>
<header>

    <div>
        <a href="<c:url value="/home" />"><img src="<c:url value="/resources/images/common/logo.png" />"></a>
    </div><!--
            --><div>
    <nav>
        <ul>
            <li id = "notification-nav">
            </li>
            <li>
                <a href="<c:url value="/home" />">Home</a>
            </li>
            <c:choose>
                <c:when test = "${empty sessionScope.username}">
                    <li>
                        <a title = "Go to Login" href="<c:url value="/login" />">Login</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a title = "Go to My Account" href="<c:url value="/account" />">My Account</a>
                    </li>
                    <li>
                        <a title = "Logout" href="#" onclick="document.logoutForm.submit()" >Logout</a>
                    </li>
                </c:otherwise>
            </c:choose>
            <li>
                <a href="<c:url value="/help/general" />">Help</a>
            </li>
        </ul>
    </nav>
</div>
</header>
<form method = "get">
    <table>
        <tr>
            <td id = "icon-table-cell">
                <img id = "magnifier-image" src="<c:url value="/resources/images/common/magnifier.png" />">
            </td>
            <td id = "search-table-cell">
                <input type="text" id = "search-input" autocomplete="off" placeholder="Search for a movie, actor, director..."/>
            </td>
        </tr>
    </table>
    <div id = "absolute-div">
        <ul id = "search-results">
        </ul>
    </div>
</form>
<section class = "body-content">

    <!-- Content Start -->
    <div>
        <h2>
            Slideshow for ${title}:
        </h2>

        <div class="w3-content w3-display-container">
            <c:forEach var="o" items="${images}" varStatus="loop" >

                <img class="mySlides" src="<c:url value="${o}" />" >

                <c:if test="${not empty sessionScope.username}">

                    <c:set var="contains" value="false" />

                    <c:forEach var="item" items="${sessionScope.images}">
                        <c:if test="${item == o}">
                            <c:set var="contains" value="true" />
                        </c:if>
                    </c:forEach>

                    <c:choose>
                        <c:when test="${contains == true}">
                            <img title = "Add/Remove Image To Favorites" class = "focus-icon" src="<c:url value="/resources/images/icons/correct.png" />" onclick="imageFavorite(this, '${o}')">
                        </c:when>
                        <c:otherwise>
                            <img title = "Add/Remove Image To Favorites" class = "focus-icon" src="<c:url value="/resources/images/icons/wishlist.png" />" onclick="imageFavorite(this, '${o}')">
                        </c:otherwise>
                    </c:choose>

                </c:if>

            </c:forEach>

            <a class="w3-btn-floating w3-display-left" onclick="plusDivs(-1)">&#10094;</a>
            <a class="w3-btn-floating w3-display-right" onclick="plusDivs(1)">&#10095;</a>
        </div>

    </div>


    <!-- Content End -->

</section>

<footer>
    <span>Copyright &copy; 2016 - 2017 Dan-Ovidiu Graur</span>
</footer>

</body>
</html>
