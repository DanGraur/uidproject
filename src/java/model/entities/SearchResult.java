package model.entities;

/**
 * Created by noi on 12/26/2016.
 */
public class SearchResult {
    private String imageUrl;
    private String hrefUrl;
    private String text;

    public SearchResult(String imageUrl, String hrefUrl, String text) {
        this.imageUrl = imageUrl;
        this.hrefUrl = hrefUrl;
        this.text = text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getHrefUrl() {
        return hrefUrl;
    }

    public void setHrefUrl(String hrefUrl) {
        this.hrefUrl = hrefUrl;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
