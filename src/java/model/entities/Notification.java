package model.entities;

/**
 * Created by noi on 12/27/2016.
 */
public class Notification {
    private String url;
    private String text;

    public Notification(String url, String text) {
        this.url = url;
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
