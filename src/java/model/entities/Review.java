package model.entities;

/**
 * Created by noi on 12/27/2016.
 */
public class Review {
    private String username;
    private boolean isUserReview;
    private String review;

    public Review(String username, boolean isUserReview, String review) {
        this.username = username;
        this.isUserReview = isUserReview;
        this.review = review;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isUserReview() {
        return isUserReview;
    }

    public void setUserReview(boolean userReview) {
        isUserReview = userReview;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
