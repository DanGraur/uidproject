package model.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by noi on 12/29/2016.
 */
public class CinemaTicketOptions {
    List<String> cinemas;
    List<String> dates;
    List<String> hours;

    public CinemaTicketOptions() {
        cinemas = new ArrayList<>();
        dates = new ArrayList<>();
        hours = new ArrayList<>();

        cinemas.add("Cinema 1");
        cinemas.add("Cinema 2");
        cinemas.add("Cinema 3");

        dates.add("28/12/16 - wed");
        dates.add("29/12/16 - thu");
        dates.add("30/12/16 - fri");
        dates.add("31/12/16 - sat");
        dates.add("1/1/17 - sun");
        dates.add("2/1/17 - mon");

        hours.add("12:00");
        hours.add("14:00");
        hours.add("16:00");
        hours.add("18:00");
        hours.add("20:00");
        hours.add("22:00");
    }

    public List<String> getCinemas() {
        return cinemas;
    }

    public void setCinemas(List<String> cinemas) {
        this.cinemas = cinemas;
    }

    public List<String> getDates() {
        return dates;
    }

    public void setDates(List<String> dates) {
        this.dates = dates;
    }

    public List<String> getHours() {
        return hours;
    }

    public void setHours(List<String> hours) {
        this.hours = hours;
    }
}
