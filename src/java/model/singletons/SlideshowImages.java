package model.singletons;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by noi on 12/25/2016.
 */
public class SlideshowImages {
    private List<String> imagePaths;

    private static SlideshowImages ourInstance = new SlideshowImages();

    public static SlideshowImages getInstance() {
        return ourInstance;
    }

    private SlideshowImages() {
        imagePaths = new ArrayList<>();

        imagePaths.add("/resources/images/film/Stars/daisy.jpg");
        imagePaths.add("/resources/images/film/Stars/boyega.jpg");
        imagePaths.add("/resources/images/film/RandomPics/1.jpg");
        imagePaths.add("/resources/images/film/RandomPics/3.jpg");
        imagePaths.add("/resources/images/film/RandomPics/4.jpg");
        imagePaths.add("/resources/images/film/RandomPics/5.jpg");
    }

    public List<String> getImagePaths() {
        return imagePaths;
    }

    public void setImagePaths(List<String> imagePaths) {
        this.imagePaths = imagePaths;
    }
}
