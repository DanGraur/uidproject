package model.singletons;

import model.entities.Review;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by noi on 12/27/2016.
 */
public class FilmReviews {

    private static FilmReviews ourInstance = new FilmReviews();

    private List<Review> reviewList;

    public static FilmReviews getInstance() {
        return ourInstance;
    }

    private FilmReviews() {
        reviewList = new ArrayList<>();

        Review reviewOne = new Review("John Doe", false, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean at mauris vitae neque iaculis euismod ac vel ante. Ut eget neque dui. Aenean semper, enim eu dictum sodales, augue magna luctus turpis, sit amet eleifend sapien neque ac sapien. Nulla ultricies congue nibh. Aliquam quis arcu ut lorem consequat condimentum. Aliquam et dolor vel elit volutpat vehicula. Mauris vestibulum sem non nisi viverra, sit amet dictum erat ullamcorper. Ut pellentesque fringilla volutpat. Quisque lobortis tortor sit amet vehicula pharetra. Integer fermentum at lorem non lobortis.");
        Review reviewTwo = new Review("Jane Doe", false, "Suspendisse maximus nisl eget felis congue suscipit. Donec quis turpis ac nisl sodales cursus. Curabitur quis quam consequat, laoreet nisl quis, rhoncus arcu. Donec sed dui bibendum, fermentum elit a, varius odio. Nunc posuere odio lacus, in bibendum lorem tempus in. Mauris laoreet vestibulum arcu non cursus. Fusce dignissim ante vel metus eleifend, ac commodo augue eleifend. Suspendisse ut libero ac justo hendrerit interdum. Donec mollis lacinia bibendum. Cras ac ante sem. Nullam sollicitudin sit amet lorem sit amet rutrum.");

        reviewList.add(reviewOne);
        reviewList.add(reviewTwo);
    }

    public List<Review> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
    }
}
