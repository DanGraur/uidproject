package model.singletons;

import model.entities.Notification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by noi on 12/27/2016.
 */
public class NotificationSystem {
    private static NotificationSystem ourInstance = new NotificationSystem();

    private Map<String, List<Notification>> notifications;

    public static NotificationSystem getInstance() {
        return ourInstance;
    }

    private NotificationSystem() {
        notifications = new HashMap<>();

        List<Notification> johnList = new ArrayList<>();
        List<Notification> maryList = new ArrayList<>();
        List<Notification> jackList = new ArrayList<>();

        notifications.put("john", johnList);
        notifications.put("mary", maryList);
        notifications.put("jack", jackList);
    }

    public Map<String, List<Notification>> getNotifications() {
        return notifications;
    }

    public void setNotifications(Map<String, List<Notification>> notifications) {
        this.notifications = notifications;
    }
}
