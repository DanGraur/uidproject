package model.singletons;

/**
 * Created by noi on 12/29/2016.
 */
public class CardInformation {
    private String name;
    private String cardNumber;

    private static CardInformation ourInstance = new CardInformation();

    public static CardInformation getInstance() {
        return ourInstance;
    }

    private CardInformation() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
