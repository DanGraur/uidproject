package model.singletons;

import model.entities.Comment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by noi on 12/25/2016.
 */
public class ArticleComments {
        private List<Comment> commentsList;

        private static ArticleComments instance;

        private ArticleComments() {
            commentsList = new ArrayList<>();

            Comment commentOne = new Comment("John Doe", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean at mauris vitae neque iaculis euismod ac vel ante. Ut eget neque dui. ");
            Comment commentTwo = new Comment("Jane Doe", "Aenean semper, enim eu dictum sodales, augue magna luctus turpis, sit amet eleifend sapien neque ac sapien.");
            Comment commentThree = new Comment("anonymous", " Nulla ultricies congue nibh. Aliquam quis arcu ut lorem consequat condimentum. Aliquam et dolor vel elit volutpat vehicula.");

            commentsList.add(commentOne);
            commentsList.add(commentTwo);
            commentsList.add(commentThree);
        }

    public static ArticleComments getInstance() {
            if (instance == null)
                instance = new ArticleComments();

            return instance;
    }

    public List<Comment> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<Comment> commentsList) {
        this.commentsList = commentsList;
    }
}
