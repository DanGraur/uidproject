package web.controllers;

import model.entities.CinemaTicketOptions;
import model.entities.Notification;
import model.entities.SearchResult;
import model.singletons.CardInformation;
import model.singletons.NotificationSystem;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.smartcardio.Card;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by noi on 12/26/2016.
 */
@Controller
@RequestMapping(value = "/general")
public class GeneralController {

    @RequestMapping(value = "logout")
    public String logout(HttpServletRequest request) {
        String page = "home";

        request.getSession().invalidate();

        if (request.getHeader("Referer") != null || !request.getHeader("Referer").isEmpty())
            page = request.getHeader("Referer");

        return "redirect:" + page;
    }


    @ResponseBody
    @RequestMapping(value = "search-bar", method = RequestMethod.GET, produces="application/json")
    public List<SearchResult> getSearchResults(@RequestParam(value = "query") String query) {
        List<SearchResult> list = new ArrayList<>();

        query = query.toLowerCase();

        if (query.startsWith("benedict"))
            list.add(new SearchResult("/resources/images/actor/benedict.jpg", "/actor", "Benedict Cumberbatch, Actor, 1976"));
        else if (query.startsWith("star"))
            list.add(new SearchResult("/resources/images/home/BoxOffice/sw.jpg", "/film", "Star Wars: The Force Awakens, Movie, 2015"));
        else {
            list.add(new SearchResult("/resources/images/home/BoxOffice/sw.jpg", "/film", "Star Wars: The Force Awakens, Movie, 2015"));
            list.add(new SearchResult("/resources/images/actor/benedict.jpg", "/actor", "Benedict Cumberbatch, Actor, 1976"));
        }

        return list;
    }

    @ResponseBody
    @RequestMapping(value = "username")
    public String getUsername(HttpServletRequest request) {
        HttpSession userSession = request.getSession();

        String username = "";

        if (userSession.getAttribute("username") != null)
            username = (String) userSession.getAttribute("username");

        return username;
    }


    @ResponseBody
    @RequestMapping(value = "favorite-film")
    public void favoriteImage(@RequestParam(value = "name") String filmName,
                              HttpServletRequest request) {
        HttpSession userSession = request.getSession();
        Set<String> films = (Set<String>) userSession.getAttribute("films");

        if (films == null)
            films = new HashSet<>();

        if (films.contains(filmName))
            films.remove(filmName);
        else
            films.add(filmName);

        userSession.setAttribute("films", films);

        // Test:
        for (String s : films)
            System.out.println(s);
    }


    @ResponseBody
    @RequestMapping(value = "favorite-actor")
    public void favoriteActor(@RequestParam(value = "name") String actorName,
                              HttpServletRequest request) {
        HttpSession userSession = request.getSession();
        Set<String> actors = (Set<String>) userSession.getAttribute("actors");

        if (actors == null)
            actors = new HashSet<>();

        if (actors.contains(actorName)) {
            actors.remove(actorName);

            // Set notification
            NotificationSystem
                    .getInstance()
                    .getNotifications()
                    .get((String) userSession.getAttribute("username"))
                    .add(new Notification("/actor", "You've removed " + actorName + " from your Favorites List"));
        } else {
            actors.add(actorName);

            // Set notification
            NotificationSystem
                    .getInstance()
                    .getNotifications()
                    .get((String) userSession.getAttribute("username"))
                    .add(new Notification("/actor", "You've added " + actorName + " to your Favorites List"));
        }

        userSession.setAttribute("actors", actors);


        // Test:
        for (String s : actors)
            System.out.println(s);
    }


    @ResponseBody
    @RequestMapping(value = "notification")
    public String checkNotifications(HttpServletRequest request) {
        HttpSession userSession = request.getSession();

        if (userSession.getAttribute("username") == null ||
            NotificationSystem.getInstance().getNotifications().get((String) userSession.getAttribute("username")).isEmpty())
            return "";

        return "notify";
    }


    @ResponseBody
    @RequestMapping(value = "tickets/cinemas", produces="application/json")
    public List<String> getCinemaOptions() {
        return new CinemaTicketOptions().getCinemas();
    }

    @ResponseBody
    @RequestMapping(value = "tickets/date", produces="application/json")
    public List<String> getDateOptions(@RequestParam(value = "name") String name) {
        List<String> dates = new CinemaTicketOptions().getDates();

        if (name.equals("Cinema 1"))
            return dates.subList(0, 2);

        if (name.equals("Cinema 2"))
            return dates.subList(2, 4);

        return dates.subList(4, 6);
    }


    @ResponseBody
    @RequestMapping(value = "tickets/hour", produces="application/json")
    public List<String> getHourOptions(@RequestParam(value = "name") String name) {
        List<String> hours = new CinemaTicketOptions().getHours();

        if (name.equals("28/12/16 - wed") || name.equals("1/1/17 - sun"))
            return hours.subList(0, 2);

        if (name.equals("29/12/16 - thu") || name.equals("31/12/16 - sat"))
            return hours.subList(2, 4);

        return hours.subList(4, 6);
    }


    @ResponseBody
    @RequestMapping(value = "tickets/send-data", produces="application/json")
    public CardInformation saveInfoAndRetrieveCardCache(@RequestParam(value = "cinema") String cinema,
                                                        @RequestParam(value = "date") String date,
                                                        @RequestParam(value = "hour") String hour,
                                                        @RequestParam(value = "count") String count) {
        System.out.println(cinema + " " + date + " " + hour + " " + count);

        return CardInformation.getInstance();
    }


    @ResponseBody
    @RequestMapping(value = "tickets/send-card-info")
    public void receiveCardInfoAndCache(@RequestParam(value = "username") String username,
                                        @RequestParam(value = "cardNumber") String cardNumber) {
        System.out.println(username + " " + cardNumber + " ");

        CardInformation.getInstance().setName(username);
        CardInformation.getInstance().setCardNumber(cardNumber);

    }
}
