package web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by noi on 12/25/2016.
 */
@Controller
@RequestMapping(value = "/box-office")
public class BoxOfficeController {

    @RequestMapping(method = RequestMethod.GET)
    public String displayBoxOffice() {
        return "box-office";
    }

}
