package web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by noi on 12/24/2016.
 */
@Controller
@RequestMapping(value = "/actor")
public class ActorController {

    @RequestMapping(method = RequestMethod.GET)
    public String displayActor() {
        return "actor";
    }

}
