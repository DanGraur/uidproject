package web.controllers;

import model.singletons.ArticleComments;
import model.singletons.SlideshowImages;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.NavigableSet;
import java.util.Set;

/**
 * Created by noi on 12/25/2016.
 */
@Controller
@RequestMapping(value = "/slideshow")
public class SlideshowController {

    @RequestMapping(method = RequestMethod.GET)
    public String displaySlideshow(Model model) {

        model.addAttribute("title", "Star Wars: The Force Awakens");
        model.addAttribute("images", SlideshowImages.getInstance().getImagePaths());

        return "slideshow";
    }


    @ResponseBody
    @RequestMapping(value = "favorite")
    public void favoriteImage(@RequestParam(value = "path") String imagePath,
                              HttpServletRequest request) {
        HttpSession userSession = request.getSession();
        Set<String> images = (Set<String>) userSession.getAttribute("images");

        if (images == null)
            images = new HashSet<>();

        if (images.contains(imagePath))
            images.remove(imagePath);
        else
            images.add(imagePath);

        userSession.setAttribute("images", images);
    }

}
