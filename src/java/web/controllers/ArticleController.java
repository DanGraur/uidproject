package web.controllers;

import model.entities.Comment;
import model.entities.Review;
import model.singletons.ArticleComments;
import model.singletons.FilmReviews;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by noi on 12/25/2016.
 */
@Controller
@RequestMapping(value = "/article")
public class ArticleController {

    @RequestMapping(method = RequestMethod.GET)
    public String displayArticle(Model model) {

        model.addAttribute("comments", ArticleComments.getInstance().getCommentsList());

        return "article";
    }


    @RequestMapping(method = RequestMethod.POST, params = "submit-comment-button")
    public String postComment(@RequestParam(value = "comment-input") String comment,
                             HttpServletRequest request,
                             Model model) {
        HttpSession userSession = request.getSession();
        String username = userSession.getAttribute("username") == null ? "Anonymous" : (String) userSession.getAttribute("username");

        ArticleComments.getInstance().getCommentsList().add(new Comment(username, comment));

        model.addAttribute("comments", ArticleComments.getInstance().getCommentsList());

        return "article";
    }

    @ResponseBody
    @RequestMapping(value = "comment")
    public void postCommentAjax(@RequestParam(value = "comment-input") String comment,
                                  HttpServletRequest request) {
        HttpSession userSession = request.getSession();
        String username = userSession.getAttribute("username") == null ? "Anonymous" : (String) userSession.getAttribute("username");

        ArticleComments.getInstance().getCommentsList().add(new Comment(username, comment));
    }
}
