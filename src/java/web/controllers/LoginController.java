package web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by noi on 12/24/2016.
 */
@Controller
@RequestMapping(value = "/login")
public class LoginController {

    @RequestMapping(method = RequestMethod.GET)
    public String displayLogin() {
        return "login";
    }

    @RequestMapping(method = RequestMethod.POST, params = "login-button")
    public String login(@RequestParam(value = "username") String username,
                        @RequestParam(value = "password") String password,
                        HttpServletRequest request,
                        Model model) {
        HttpSession userSession = request.getSession();

        if ((username.equals("john") || username.equals("mary") || username.equals("jack")) &&
            password.equals("pass")) {
            userSession.setAttribute("username", username);

            return "home";
        }

        model.addAttribute("errMsg", "Username or Password are wrong, please try again!");

        return "login";
    }
}
