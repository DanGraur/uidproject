package web.controllers;

import model.entities.Notification;
import model.singletons.NotificationSystem;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by noi on 12/27/2016.
 */
@Controller
@RequestMapping(value = "/account")
public class AccountController {

    @RequestMapping(method = RequestMethod.GET)
    public String displayAccount(HttpServletRequest request,
                                 Model model) {

        HttpSession userSession = request.getSession();

        Set<String> films = (Set<String>) userSession.getAttribute("films");
        Set<String> actualFilms = new HashSet<>();


        if (films != null)
            for (String film : films)
                switch (film) {
                    case "skyfall":
                        actualFilms.add("Skyfall");
                        break;
                    case "fantastic-beasts":
                        actualFilms.add("Fantastic Beasts");
                        break;
                    case "black-swan":
                        actualFilms.add("Black Swan");
                        break;
                    case "sw":
                        actualFilms.add("Star Wars: The Force Awakens");
                        break;
                    case "interstellar":
                        actualFilms.add("Interstellar");
                        break;
                    case "inception":
                        actualFilms.add("Inception");
                        break;
                    case "rogue-one":
                        actualFilms.add("Star Wars: Rogue One");
                        break;
                    case "prometheus":
                        actualFilms.add("Prometheus");
                        break;
                    case "guardians":
                        actualFilms.add("Guardians of the Galaxy");
                        break;
                    case "strange":
                        actualFilms.add("Dr. Strange");
                        break;

                }

        model.addAttribute("actualFilms", actualFilms.isEmpty() ? null : actualFilms);

        return "account";
    }


    @RequestMapping(value = "notifications", method = RequestMethod.GET)
    public String displayNotifications(HttpServletRequest request,
                                       Model model) {

        if (request.getSession().getAttribute("username") == null)
            return "home";

        List<Notification> copyNotifications =
                new ArrayList<>(NotificationSystem.getInstance().getNotifications().get((String) request.getSession().getAttribute("username")));

        model.addAttribute("notifications", copyNotifications);

        NotificationSystem.getInstance().getNotifications().get((String) request.getSession().getAttribute("username")).clear();

        return "notifications";
    }
}
