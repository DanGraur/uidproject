package web.controllers;

import model.entities.Notification;
import model.entities.Review;
import model.singletons.FilmReviews;
import model.singletons.NotificationSystem;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by noi on 12/24/2016.
 */
@Controller
@RequestMapping(value = "/film")
public class FilmController {

    private List<String> getOtherFriends(Object obj) {
        if (obj == null)
            return null;

        String name = (String) obj;

        List<String> list = new ArrayList<>();

        if (!name.equals("john"))
            list.add("john");

        if (!name.equals("mary"))
            list.add("mary");

        if (!name.equals("jack"))
            list.add("jack");

        return list;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String displayFilm(HttpServletRequest request,
                              Model model) {
        HttpSession userSession = request.getSession();

        userSession.setAttribute("hasReview", false);

        for (Review r : FilmReviews.getInstance().getReviewList())
            if (r.isUserReview())
                userSession.setAttribute("hasReview", true);

        model.addAttribute("reviews", FilmReviews.getInstance().getReviewList());
        model.addAttribute("friends", getOtherFriends(userSession.getAttribute("username")));

        return "film";
    }


    @RequestMapping(method = RequestMethod.POST, params = "submit-review-button")
    public String postReview(@RequestParam(value = "review-input") String review,
                             HttpServletRequest request,
                             Model model) {
        HttpSession userSession = request.getSession();

        userSession.setAttribute("hasReview", true);

        FilmReviews.getInstance().getReviewList().add(new Review((String) userSession.getAttribute("username"), true, review));

        model.addAttribute("reviews", FilmReviews.getInstance().getReviewList());
        model.addAttribute("friends", getOtherFriends(userSession.getAttribute("username")));

        return "film";
    }


    @RequestMapping(method = RequestMethod.POST, params = "delete-review-button")
    public String deleteReview(HttpServletRequest request,
                               Model model) {
        HttpSession userSession = request.getSession();

        userSession.setAttribute("hasReview", false);

        Iterator<Review> iterator = FilmReviews.getInstance().getReviewList().iterator();

        while(iterator.hasNext())
            if (iterator.next().isUserReview())
                iterator.remove();

        model.addAttribute("reviews", FilmReviews.getInstance().getReviewList());
        model.addAttribute("friends", getOtherFriends(userSession.getAttribute("username")));

        return "film";
    }

    @ResponseBody
    @RequestMapping(value = "recommend")
    public void recommendFilm(@RequestParam(value = "name") String friendName,
                               HttpServletRequest request) {
        HttpSession userSession = request.getSession();

        NotificationSystem
                .getInstance()
                .getNotifications()
                .get(friendName)
                .add(new Notification("/film", ((String) userSession.getAttribute("username")) + " has recommended Star Wars: The Force Awakens"));
    }
}
