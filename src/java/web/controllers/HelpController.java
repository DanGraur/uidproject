package web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by noi on 12/25/2016.
 */
@Controller
@RequestMapping(value = "/help")
public class HelpController {

    @RequestMapping(value = "general", method = RequestMethod.GET)
    public String displayGeneralHelpPage() {
        return "help/help-general";
    }


    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String displayLoginHelpPage() {
        return "help/help-login";
    }

    @RequestMapping(value = "actor", method = RequestMethod.GET)
    public String displayActorHelpPage() {
        return "help/help-actor";
    }
}
