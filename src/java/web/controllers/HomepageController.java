package web.controllers;

import model.entities.SearchResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by noi on 12/24/2016.
 */
@Controller
@RequestMapping(value = "/home")
public class HomepageController {

    @RequestMapping(method = RequestMethod.GET)
    public String displayHome() {
        return "home";
    }


}
